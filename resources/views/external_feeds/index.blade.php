@extends('layout')

@section('title', 'External Feeds')
@section('content')
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                               External Feeds
                            </h3>
                            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                            <div class="kt-subheader__group" id="kt_subheader_search">
										<span class="kt-subheader__desc" id="kt_subheader_total">
											450 Total </span>
                                <form class="kt-margin-l-20" id="kt_subheader_search_form">
                                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                                        <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
													<span>
														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																<rect x="0" y="0" width="24" height="24" />
																<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
															</g>
														</svg>

                                                        <!--<i class="flaticon2-search-1"></i>-->
													</span>
												</span>
                                    </div>
                                </form>
                                <!--start serach -->
                                <form class="kt-form kt-form--fit kt-form--label-right">
                                    <div class="kt-portlet__body">
                                        <div class="kt-form__group kt-form__group--inline">
                                                <select class="form-control kt-select2" id="kt_select2_1" name="param">
                                                    <option></option>
                                                    @foreach($publishers as $pub)
                                                    <option value="{{$pub->id}}">{{$pub->company}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="kt-form__label" style="padding-left: 20px;">
                                                    <label>Category:</label>
                                                </div>
                                                <select  class="form-control kt-selectpicker" id="kt_form_category" name="category"  multiple>
                                                    <option value="category1">category1</option>
                                                    <option value="category2">category2</option>
                                                    <option value="category3">category3</option>
                                                </select>
                                                <div class="kt-form__label" style="padding-left: 20px;">
                                                    <label>Status:</label>
                                                </div>
                                                <select  class="form-control kt-selectpicker" id="kt_form_status" name="Status" title="Select a Status">
                                                    <option value="">All</option>
                                                    <option value="active">Active</option>
                                                    <option value="inactive">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                </form>

                                <!-- End search-->
                            </div>
                            <div class="kt-subheader__group kt-hidden" id="kt_subheader_group_actions">
                                <div class="kt-subheader__desc"><span id="kt_subheader_group_selected_rows"></span> Selected:</div>
                                <div class="btn-toolbar kt-margin-l-20">
                                    <div class="dropdown" id="kt_subheader_group_actions_status_change">
                                        <button type="button" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
                                            Update Status
                                        </button>
                                        <div class="dropdown-menu">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__section kt-nav__section--first">
                                                    <span class="kt-nav__section-text">Change status to:</span>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="1">
                                                        <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-success kt-badge--inline kt-badge--bold">Approved</span></span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="2">
                                                        <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--bold">Rejected</span></span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="3">
                                                        <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--bold">Pending</span></span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="4">
                                                        <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-info kt-badge--inline kt-badge--bold">On Hold</span></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <button class="btn btn-label-success btn-bold btn-sm btn-icon-h" id="kt_subheader_group_actions_fetch" data-toggle="modal" data-target="#kt_datatable_records_fetch_modal">
                                        Fetch Selected
                                    </button>
                                    <button class="btn btn-label-danger btn-bold btn-sm btn-icon-h" id="kt_subheader_group_actions_delete_all">
                                        Delete All
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <a href="#" class="">
                            </a>
                            <a href="{{route('externalFeeds.web.create')}}" class="btn btn-label-brand btn-bold">
                                Add External Feed </a>
                            <div class="kt-subheader__wrapper">
                                <div class="dropdown dropdown-inline" data-toggle="kt-tooltip-" title="Quick actions" data-placement="left">
                                    <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />
                                            </g>
                                        </svg>

                                        <!--<i class="flaticon2-plus"></i>-->
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

                                        <!--begin::Nav-->
                                        <ul class="kt-nav">
                                            <li class="kt-nav__head">
                                                Add New:
                                                <i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
                                            </li>
                                            <li class="kt-nav__separator"></li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-drop"></i>
                                                    <span class="kt-nav__link-text">Orders</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-new-email"></i>
                                                    <span class="kt-nav__link-text">Members</span>
                                                    <span class="kt-nav__link-badge">
																<span class="kt-badge kt-badge--brand kt-badge--rounded">15</span>
															</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-calendar-8"></i>
                                                    <span class="kt-nav__link-text">Reports</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-link"></i>
                                                    <span class="kt-nav__link-text">Finance</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__separator"></li>
                                            <li class="kt-nav__foot">
                                                <a class="btn btn-label-brand btn-bold btn-sm" href="#">More options</a>
                                                <a class="btn btn-clean btn-bold btn-sm kt-hidden" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
                                            </li>
                                        </ul>

                                        <!--end::Nav-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->
                <!-- begin:: Content -->
                    <!--begin::Portlet-->
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__body kt-portlet__body--fit">

                            <!--begin: Datatable -->

                         <div class="kt-datatable" id="kt_apps_user_list_datatable"></div>

                    <!--end::Portlet-->



                <!-- end:: Content -->

        </div>
    </div>
</div>

<!-- end:: Page -->
@endsection
@push('scripts')
<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#2c77f4",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };



</script>

<!-- end::Global Config -->

<script src="assets/js/pages/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
<!--begin::Page Scripts(used by this page) -->
{{--<script src="datatable-external_feeds.js" type="text/javascript"></script>--}}

<script type="text/javascript">
    $(document).ready(function(){
    var KTUserListDatatable = function() {

        // variables
        var datatable;

        // init
        var init = function() {
            // init the datatables. Learn more: https://keenthemes.com/metronic/?page=docs&section=datatable
            datatable = $('#kt_apps_user_list_datatable').KTDatatable({


                // datasource definition
                processing: true,
                serverSide: true,
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            method:'GET',
                            url: '{{route("externalFeeds.index")}}',
                            params: {
                                advertiser_id: null
                            }
                        },
                    },
                    pageSize: 25, // display 15 records per page
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    sort: {
                        field: 'updated_at',
                        sort: 'desc'
                    }
                },

                // layout definition
                layout: {
                    scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                    footer: false, // display/hide footer
                },

                // column sorting
                sortable: true,

                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 1000,
                },

                // columns definition
                columns: [{
                    field: 'RecordID',
                    title: '#',
                    sortable: false,
                    width: 20,
                    selector: {
                        class: 'kt-checkbox--solid'
                    },
                    textAlign: 'center',
                }, {
                    data: "data",
                    field: "id",
                    title: "ID",
                    width:300

                },{
                    data: "data",
                    field: "advertiser_id",
                    title: "Advertiser id",

                },
                    {
                        data: "data",
                        field: "company",
                        title: "Advertiser Company",

                    },  {
                        data: "data",
                        field: "category",
                        title: "Category",

                    },{
                        data: "data",
                        field: 'is_active',
                        title: 'Status',
                        width: 100
                    }, {
                        data: "data",
                        field: 'pub_companies',
                        title: 'Publishers Companies',
                    },
                    {
                        field: "Actions",
                        width: 80,
                        title: "Actions",
                        sortable: false,
                        autoHide: false,
                        overflow: 'visible',
                        template: function(data) {
                            return '\
                           <div class="dropdown">\
                               <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">\
                                   <i class="flaticon-more-1"></i>\
                               </a>\
                               <div class="dropdown-menu dropdown-menu-right">\
                                   <ul class="kt-nav">\
                                       <li class="kt-nav__item">\
                                           <a href="#" class="kt-nav__link">\
                                               <i class="kt-nav__link-icon flaticon2-expand"></i>\
                                               <span class="kt-nav__link-text">View</span>\
                                           </a>\
                                       </li>\
                                       <li class="kt-nav__item">\
                                           <a href="externalFeeds/' +  data.id  + '/edit" class="kt-nav__link">\
                                               <i class="kt-nav__link-icon flaticon2-contract"></i>\
                                               <span class="kt-nav__link-text">Edit</span>\
                                           </a>\
                                       </li>\
                                       <li class="kt-nav__item">\
                                           <a href="#" class="kt-nav__link">\
                                               <i class="kt-nav__link-icon flaticon2-trash"></i>\
                                               <span class="kt-nav__link-text">Delete</span>\
                                           </a>\
                                       </li>\
                                       <li class="kt-nav__item">\
                                           <a href="#" class="kt-nav__link">\
                                               <i class="kt-nav__link-icon flaticon2-mail-1"></i>\
                                               <span class="kt-nav__link-text">Export</span>\
                                           </a>\
                                       </li>\
                                   </ul>\
                               </div>\
                           </div>\
                       ';
                        },

                    }]
            });
        };







        // selection
        var selection = function() {
            // init form controls
            //$('#kt_form_status, #kt_form_type').selectpicker();

            // event handler on check and uncheck on records
            datatable.on('kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',	function(e) {
                var checkedNodes = datatable.rows('.kt-datatable__row--active').nodes(); // get selected records
                var count = checkedNodes.length; // selected records count

                $('#kt_subheader_group_selected_rows').html(count);

                if (count > 0) {
                    $('#kt_subheader_search').addClass('kt-hidden');
                    $('#kt_subheader_group_actions').removeClass('kt-hidden');
                } else {
                    $('#kt_subheader_search').removeClass('kt-hidden');
                    $('#kt_subheader_group_actions').addClass('kt-hidden');
                }
            });
        }

        // fetch selected records
        var selectedFetch = function() {
            // event handler on selected records fetch modal launch
            $('#kt_datatable_records_fetch_modal').on('show.bs.modal', function(e) {
                // show loading dialog
                var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': 'Loading ...'});
                loading.show();

                setTimeout(function() {
                    loading.hide();
                }, 1000);

                // fetch selected IDs
                var ids = datatable.rows('.kt-datatable__row--active').nodes().find('.kt-checkbox--single > [type="checkbox"]').map(function(i, chk) {
                    return $(chk).val();
                });

                // populate selected IDs
                var c = document.createDocumentFragment();

                for (var i = 0; i < ids.length; i++) {
                    var li = document.createElement('li');
                    li.setAttribute('data-id', ids[i]);
                    li.innerHTML = 'Selected record ID: ' + ids[i];
                    c.appendChild(li);
                }

                $(e.target).find('#kt_apps_user_fetch_records_selected').append(c);
            }).on('hide.bs.modal', function(e) {
                $(e.target).find('#kt_apps_user_fetch_records_selected').empty();
            });
        };

        // selected records status update
        var selectedStatusUpdate = function() {
            $('#kt_subheader_group_actions_status_change').on('click', "[data-toggle='status-change']", function() {
                var status = $(this).find(".kt-nav__link-text").html();

                // fetch selected IDs
                var ids = datatable.rows('.kt-datatable__row--active').nodes().find('.kt-checkbox--single > [type="checkbox"]').map(function(i, chk) {
                    return $(chk).val();
                });

                if (ids.length > 0) {
                    // learn more: https://sweetalert2.github.io/
                    swal.fire({
                        buttonsStyling: false,

                        html: "Are you sure to update " + ids.length + " selected records status to " + status + " ?",
                        type: "info",

                        confirmButtonText: "Yes, update!",
                        confirmButtonClass: "btn btn-sm btn-bold btn-brand",

                        showCancelButton: true,
                        cancelButtonText: "No, cancel",
                        cancelButtonClass: "btn btn-sm btn-bold btn-default"
                    }).then(function(result) {
                        if (result.value) {
                            swal.fire({
                                title: 'Deleted!',
                                text: 'Your selected records statuses have been updated!',
                                type: 'success',
                                buttonsStyling: false,
                                confirmButtonText: "OK",
                                confirmButtonClass: "btn btn-sm btn-bold btn-brand",
                            })
                            // result.dismiss can be 'cancel', 'overlay',
                            // 'close', and 'timer'
                        } else if (result.dismiss === 'cancel') {
                            swal.fire({
                                title: 'Cancelled',
                                text: 'You selected records statuses have not been updated!',
                                type: 'error',
                                buttonsStyling: false,
                                confirmButtonText: "OK",
                                confirmButtonClass: "btn btn-sm btn-bold btn-brand",
                            });
                        }
                    });
                }
            });
        }

        // selected records delete
        var selectedDelete = function() {
            $('#kt_subheader_group_actions_delete_all').on('click', function() {
                // fetch selected IDs
                var ids = datatable.rows('.kt-datatable__row--active').nodes().find('.kt-checkbox--single > [type="checkbox"]').map(function(i, chk) {
                    return $(chk).val();
                });

                if (ids.length > 0) {
                    // learn more: https://sweetalert2.github.io/
                    swal.fire({
                        buttonsStyling: false,

                        text: "Are you sure to delete " + ids.length + " selected records ?",
                        type: "danger",

                        confirmButtonText: "Yes, delete!",
                        confirmButtonClass: "btn btn-sm btn-bold btn-danger",

                        showCancelButton: true,
                        cancelButtonText: "No, cancel",
                        cancelButtonClass: "btn btn-sm btn-bold btn-brand"
                    }).then(function(result) {
                        if (result.value) {
                            swal.fire({
                                title: 'Deleted!',
                                text: 'Your selected records have been deleted! :(',
                                type: 'success',
                                buttonsStyling: false,
                                confirmButtonText: "OK",
                                confirmButtonClass: "btn btn-sm btn-bold btn-brand",
                            })
                            // result.dismiss can be 'cancel', 'overlay',
                            // 'close', and 'timer'
                        } else if (result.dismiss === 'cancel') {
                            swal.fire({
                                title: 'Cancelled',
                                text: 'You selected records have not been deleted! :)',
                                type: 'error',
                                buttonsStyling: false,
                                confirmButtonText: "OK",
                                confirmButtonClass: "btn btn-sm btn-bold btn-brand",
                            });
                        }
                    });
                }
            });
        }

        var updateTotal = function() {
            datatable.on('kt-datatable--on-layout-updated', function () {
                //$('#kt_subheader_total').html(datatable.getTotalRows() + ' Total');
            });
        };


        $("#kt_select2_1").select2(
            {
                placeholder: "Select a Publisher",
                allowClear: true
            });

        $('#kt_select2_1').on('select2:select', function (e) {
            datatable.options.data.source.read.params.advertiser_id = e.params.data.id;

            datatable.reload();
        });

        $('#kt_select2_1').on('select2:clear', function (e) {
            datatable.options.data.source.read.params.advertiser_id = null;

            datatable.reload();
        });

        //filtr category
        $( "#kt_form_category" ).change(function() {

            datatable.options.data.source.read.params.category = $(this).val();

            datatable.reload();

        });

        //filtr status

        $( "#kt_form_status" ).change(function() {

            datatable.options.data.source.read.params.status = $(this).val();

            datatable.reload();

        });

        return {
            // public functions
            init: function() {
                init();
                search();
                selection();
                selectedFetch();
                selectedStatusUpdate();
                selectedDelete();
                updateTotal();
            },
        };
    }();

    // On document ready

        KTUserListDatatable.init();
    });


</script>
@endpush
