@extends('layout')

@section('title', 'Edit External Feed')

@section('content')
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                               Edit External Feed
                            </h3>
                            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                            <div class="kt-subheader__group" id="kt_subheader_search">
										<span class="kt-subheader__desc" id="kt_subheader_total">
											Enter user details and submit </span>
                            </div>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <a href="#" class="btn btn-default btn-bold">
                                Back </a>
                            <div class="btn-group">
                                <button type="button" class="btn btn-brand btn-bold">
                                    Submit </button>
                                <button type="button" class="btn btn-brand btn-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-writing"></i>
                                                <span class="kt-nav__link-text">Save &amp; continue</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-medical-records"></i>
                                                <span class="kt-nav__link-text">Save &amp; add new</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-hourglass-1"></i>
                                                <span class="kt-nav__link-text">Save &amp; exit</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    <div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">


                        <div class="kt-portlet">
                            <div class="kt-portlet__body kt-portlet__body--fit">
                                <div class="kt-grid">
                                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">

                                        <!--begin: Form Wizard Form-->
                                       <form class="kt-form" id="my_form">
                                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                <div class="kt-heading kt-heading--md"><h3>External feed Details:</h3></div>
                                                <div class="kt-section kt-section--first">
                                                    <div class="kt-wizard-v4__form">
                                                        <div class="row">
                                                            <div class="col-xl-12">
                                                                <div class="kt-section__body">
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Advertiser</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <select class="form-control" name="advertiser" id="advertiser">
                                                                                @foreach($advertisers as $advert)
                                                                                    @if($advert->company == $externalFeed->advertiser->company)
                                                                                        <option value="{{$advert->id}}" selected>{{$advert->company}}</option>
                                                                                    @else
                                                                                    <option value="{{$advert->id}}">{{$advert->company}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Name</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <input type="text" class="form-control"  name="name" id="name" value="{{old('name', $externalFeed->name)}}" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Category</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <select  id="category" class="form-control" name="category" >
                                                                                <option value="category1" @if($externalFeed->category == 'category1') selected @endif>category1</option>
                                                                                <option value="category2" @if($externalFeed->category == 'category2') selected @endif>category2</option>
                                                                                <option value="category3" @if($externalFeed->category == 'category3') selected @endif>category3</option>
                                                                            </select>
                                                                            <div class="invalid-feedback" id="category_error"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Parameters</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <input type="text" class="form-control"  name="parameters" id="parameters" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Is Active</label>
                                                                        <span class="kt-switch kt-switch--icon">
                                                                            <label>
                                                                                <input type="checkbox"  id="is_active" name="is_active" @if($externalFeed->is_active) checked="checked" @endif >
                                                                                <span></span>
                                                                            </label>
                                                                        </span>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Country Whitelist</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                <input  type="text" class="tags" id="country_whitelist_ex" name="country_whitelist" placeholder='country...' value="{{old('country_whitelist', $externalFeed->country_whitelist)}}" >
                                                                                <div class="invalid-feedback" id="c_w_l_error"></div>
                                                                                <div class="kt-margin-t-10">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Country Blacklist</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                <input  type="text" class="tags" id="country_blacklist_ex" name="country_blacklist" placeholder='country...' value="{{old('country_blacklist', $externalFeed->country_blacklist)}}" >
                                                                                <div class="invalid-feedback" id="c_b_l_error"></div>
                                                                                <div class="kt-margin-t-10">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source id Whitelist</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                <input  type="text" class="tagss" id="id_whitelist_ex"  name="id_whitelist" placeholder='country...' value="{{old('id_whitelist', $externalFeed->source_id_whitelist)}}">
                                                                                <div class="invalid-feedback" id="id_w_l_error"></div>
                                                                                <div class="kt-margin-t-10">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source id Blacklist</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                <input  type="text" class="tagss" id="id_blacklist_ex" name="id_blacklist" placeholder='country...' value="{{old('id_blacklist', $externalFeed->source_id_blacklist)}}">
                                                                                <div class="invalid-feedback" id="id_b_l_error"></div>
                                                                                <div class="kt-margin-t-10">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source url Whitelist</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                <input  type="text" class="tagss" id="url_whitelist_ex" name="url_whitelist" placeholder='country...' value="{{old('url_whitelist', $externalFeed->source_url_whitelist)}}" >
                                                                                <div class="invalid-feedback" id="url_w_l_error"></div>
                                                                                <div class="kt-margin-t-10">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source url Blacklist</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                <input  type="text" class="tagss" id="url_blacklist_ex" name="url_blacklist" placeholder='country...' value="{{old('url_blacklist', $externalFeed->source_url_blacklist)}}">
                                                                                <div class="invalid-feedback" id="url_b_l_error"></div>
                                                                                <div class="kt-margin-t-10">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="kt-heading kt-heading--md">Internal feeds:</div>
                                                                    <hr class="my-4">
                                                                   <div  id="collapse-group">
                                                                            @foreach($internalFeeds as $internal)
                                                                                       <div class="panel panel-default">
                                                                                            <div class="panel-heading">
                                                                                                <h4 class="panel-title">
                                                                                                    <span class="kt-switch kt-switch--icon">
                                                                                                        <label>
                                                                                                            <input type="checkbox" class="checkbox" id="check_{{$loop->index}}" name="{{$internal->id}}" @if($internal->is_active) checked="cheked" @endif>
                                                                                                            <span></span>
                                                                                                        </label>
                                                                                                         <div>
                                                                                                        {{$internal->internalFeed->publisher->company}}
                                                                                                        </div>
                                                                                                    </span>
                                                                                                </h4>
                                                                                                <hr class="my-4">
                                                                                            </div>
                                                                                            <div id="div_check_{{$loop->index}}" @if(!$internal->is_active) style="display:none;" @endif>
                                                                                                <div class="panel-body">
                                                                                                    <div class="form-group row">
                                                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Country Whitelist </label>
                                                                                                        <div class="col-lg-9 col-xl-9">
                                                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                                                <input type="text" class="tags" id="c_w_l{{$internal->id}}"  value="{{$internal->country_whitelist}}" placeholder='country...' >
                                                                                                                <div class="kt-margin-t-10">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group row">
                                                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Country Blacklist</label>
                                                                                                        <div class="col-lg-9 col-xl-9">
                                                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                                                <input  type="text" class="tags" id="c_b_l{{$internal->id}}" value="{{$internal->country_blacklist}}" placeholder='country...' >
                                                                                                                <div class="kt-margin-t-10">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group row">
                                                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source id Whitelist</label>
                                                                                                        <div class="col-lg-9 col-xl-9">
                                                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                                                <input  type="text"  class="tagss" id="id_w_l{{$internal->id}}" placeholder='source id...' value="{{$internal->source_id_whitelist}}">
                                                                                                                <div class="kt-margin-t-10">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group row">
                                                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source id Blacklist</label>
                                                                                                        <div class="col-lg-9 col-xl-9">
                                                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                                                <input  type="text" class="tagss" id="id_b_l{{$internal->id}}" placeholder='source id...' value="{{$internal->source_id_blacklist}}">
                                                                                                                <div class="kt-margin-t-10">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group row">
                                                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source url Whitelist</label>
                                                                                                        <div class="col-lg-9 col-xl-9">
                                                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                                                <input  type="text"  class="tagss" id="url_w_l{{$internal->id}}" placeholder='source url...' value="{{$internal->source_url_whitelist}}" >
                                                                                                                <div class="kt-margin-t-10">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group row">
                                                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source  url Blacklist</label>
                                                                                                        <div class="col-lg-9 col-xl-9">
                                                                                                            <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                                                <input  type="text" class="tagss" id="url_b_l{{$internal->id}}" placeholder='source url...' value="{{$internal->source_url_blacklist}}">
                                                                                                                <div class="kt-margin-t-10">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="kt-login__actions">
                                                <button id="kt_login_signin_submit" type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                           @csrf
                                        </form>
                                        <!--end: Form Wizard Form-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

        </div>
    </div>
</div>

<!-- end:: Page -->


@endsection
@push('css')
    <link href="/assets/css/pages/wizard/wizard-4.css" rel="stylesheet" type="text/css" />
@endpush

<!-- begin::Global Config(global config for global JS sciprts) -->
@push('scripts')

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#2c77f4",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->
<!--begin::Global Theme Bundle(used by all pages) -->
<!--end::Global Theme Bundle -->

<!--begin::Page Scripts(used by this page) -->
<script src="/assets/js/pages/custom/user/add-user.js" type="text/javascript"></script>
<script src="/tagify.js" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function(){


        var checks = document.getElementsByClassName('checkbox');
        for (let i = 0; i < checks.length; i++) {
            $("#" + checks[i].id).change(function() {
                if(this.checked != true){
                    $("#div_" + checks[i].id).hide();
                }
                else {
                    $("#div_" + checks[i].id).show();
                }
            });
        }

        for (let selector of document.querySelectorAll('input.tagss')) {
            new Tagify(selector);
        }





    $('#my_form').submit(function() {

        var checkboxes = document.getElementsByClassName('checkbox');
        var checkboxesAll = [];
        for (var index = 0; index < checkboxes.length; index++) {
            checkboxesAll.push(checkboxes[index].name); // положим в массив выбранный
        }

        var data = {};
        var key_c_v_l = 'country_whitelist';
        var key_b_v_l = 'country_blacklist';
        var key_id_v_l = 'id_whitelist';
        var key_id_b_l = 'id_blacklist';
        var key_url_v_l = 'url_whitelist';
        var key_url_b_l = 'url_blacklist';

        for (var i = 0; i <checkboxesAll.length; i++)
        {
            var key = checkboxesAll[i];
            var values = {};

            if (checkboxes[i].checked) {
                values['is_active'] = 1;
            } else {
                values['is_active'] = 0;
            }

            var country_whitelist = document.getElementById('c_w_l' + checkboxesAll[i]).value;
            var country_blacklist = document.getElementById('c_b_l' + checkboxesAll[i]).value;
            var id_whitelist = document.getElementById('id_w_l' + checkboxesAll[i]).value;
            var id_blacklist = document.getElementById('id_b_l' + checkboxesAll[i]).value;
            var url_whitelist = document.getElementById('url_w_l' + checkboxesAll[i]).value;
            var url_blacklist = document.getElementById('url_b_l' + checkboxesAll[i]).value;

            values[key_c_v_l] = country_whitelist;
            values[key_b_v_l] = country_blacklist;
            values[key_id_v_l] = id_whitelist;
            values[key_id_b_l] = id_blacklist;
            values[key_url_v_l] = url_whitelist;
            values[key_url_b_l] = url_blacklist;

            data[key] = values;
        }


        var request = {};
        var advert_id = document.getElementById('advertiser').value;
        var name = document.getElementById('name').value;
        var category = document.getElementById('category').value;
        var parameters = document.getElementById('parameters').value;
        var is_active = $("#is_active").is(':checked');
        var external_country_whitelist_ex = document.getElementById('country_whitelist_ex').value;
        var external_country_blacklist_ex = document.getElementById('country_blacklist_ex').value;
        var external_id_whitelist_ex = document.getElementById('id_whitelist_ex').value;
        var external_id_blacklist_ex = document.getElementById('id_blacklist_ex').value;
        var external_url_whitelist_ex = document.getElementById('url_whitelist_ex').value;
        var external_url_blacklist_ex = document.getElementById('url_blacklist_ex').value;

        request['advertiser_id'] = advert_id;
        request['name'] = name;
        request['category'] = category;
        request['parameters'] = parameters;

        if(is_active == true)
        {
            is_active = 1;
        } else if (is_active == false){
            is_active = 0;
        }
        request['is_active'] = is_active;
        request['country_whitelist'] = external_country_whitelist_ex;
        request['country_blacklist'] = external_country_blacklist_ex;
        request['id_whitelist'] = external_id_whitelist_ex;
        request['id_blacklist'] = external_id_blacklist_ex;
        request['url_whitelist'] = external_url_whitelist_ex;
        request['url_blacklist'] = external_url_blacklist_ex;

        request['internal_feeds'] = data;



        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $.ajax({
            url:  '{{ route('externalFeeds.update', $externalFeed->id) }}',
            type: 'PUT',
            data: request,
            error: function(xhr) {
                var response = jQuery.parseJSON(xhr.responseText);
                if(response[0].name)
                {
                    $('#name_error').html('');
                    document.getElementById('name').classList.add('is-invalid');
                    $('#name_error').css('display', 'block');
                    for(var i = 0; i < response[0].name.length; i++) {
                        $('#name_error').append(response[0].name[i]);
                    }
                }
                if(response[0].category)
                {
                    $('#category_error').html('');
                    document.getElementById('category').classList.add('is-invalid');
                    $('#category_error').css('display', 'block');
                    for(var i = 0; i < response[0].name.length; i++) {
                        $('#category_error').append(response[0].name[i]);
                    }
                }
                if(response[0].advert_id)
                {
                    $('#advertiser_error').html('');
                    document.getElementById('advertiser').classList.add('is-invalid');
                    $('#advertiser_error').css('display', 'block');
                    for(var i = 0; i < response[0].advert_id.length; i++) {
                        $('#advertiser_error').append(response[0].advert_id[i]);
                    }
                }
                if(response[0].parameters)
                {
                    $('#parameters_error').html('');
                    document.getElementById('parameters').classList.add('is-invalid');
                    $('#parameters_error').css('display', 'block');
                    for(var i = 0; i < response[0].parameters.length; i++) {
                        $('#parameters_error').append(response[0].parameters[i]);
                    }
                }
                if(response[0].is_active)
                {
                    $('#is_active_error').html('');
                    document.getElementById('is_active').classList.add('is-invalid');
                    $('#is_active_error').css('display', 'block');
                    for(var i = 0; i < response[0].is_active.length; i++) {
                        $('#is_active_error').append(response[0].is_active[i]);
                    }
                }
                if(response[0].country_whitelist)
                {
                    $('#c_w_l_error').html('');
                    document.getElementById('country_whitelist_ex').classList.add('is-invalid');
                    $('#c_w_l_error').css('display', 'block');
                    for(var i = 0; i < response[0].country_whitelist.length; i++) {
                        $('#c_w_l_error').append(response[0].country_whitelist[i]);
                    }
                }
                if(response[0].country_blacklist)
                {
                    $('#c_b_l_error').html('');
                    document.getElementById('country_blacklist_ex').classList.add('is-invalid');
                    $('#c_b_l_error').css('display', 'block');
                    for(var i = 0; i < response[0].country_blacklist.length; i++) {
                        $('#c_b_l_error').append(response[0].country_blacklist[i]);
                    }
                }
                if(response[0].id_whitelist)
                {
                    $('#id_w_l_error').html('');
                    document.getElementById('id_whitelist_ex').classList.add('is-invalid');
                    $('#id_w_l_error').css('display', 'block');
                    for(var i = 0; i < response[0].id_whitelist.length; i++) {
                        $('#id_w_l_error').append(response[0].id_whitelist[i]);
                    }
                }
                if(response[0].id_blacklist)
                {
                    $('#id_b_l_error').html('');
                    document.getElementById('id_blacklist_ex').classList.add('is-invalid');
                    $('#id_b_l_error').css('display', 'block');
                    for(var i = 0; i < response[0].id_blacklist.length; i++) {
                        $('#id_b_l_error').append(response[0].id_blacklist[i]);
                    }
                }
                if(response[0].url_whitelist)
                {
                    $('#url_w_l_error').html('');
                    document.getElementById('url_whitelist_ex').classList.add('is-invalid');
                    $('#url_w_l_error').css('display', 'block');
                    for(var i = 0; i < response[0].url_whitelist.length; i++) {
                        $('#url_w_l_error').append(response[0].url_whitelist[i]);
                    }
                }
                if(response[0].url_blacklist)
                {
                    $('#url_b_l_error').html('');
                    document.getElementById('url_blacklist_ex').classList.add('is-invalid');
                    $('#url_b_l_error').css('display', 'block');
                    for(var i = 0; i < response[0].url_blacklist.length; i++) {
                        $('#url_b_l_error').append(response[0].url_blacklist[i]);
                    }
                }
                if(response.internal_feeds)
                {
                    for (var key in response.internal_feeds) {

                        if(response.internal_feeds[key].country_whitelist)
                        {
                            $('#' + [key] +'_c_w_l_error').html('');
                            document.getElementById('c_w_l' + [key]).classList.add('is-invalid');
                            $('#' + [key] +'_c_w_l_error').css('display', 'block');
                            for(var i = 0; i < response.internal_feeds[key].country_whitelist.length; i++) {
                                $('#' + [key] +'_c_w_l_error').append(response.internal_feeds[key].country_whitelist);
                            }

                        }
                        if(response.internal_feeds[key].country_blacklist)
                        {
                            $('#' + [key] +'_c_b_l_error').html('');
                            document.getElementById('c_b_l' + [key]).classList.add('is-invalid');
                            $('#' + [key] +'_c_b_l_error').css('display', 'block');
                            for(var i = 0; i < response.internal_feeds[key].country_blacklist.length; i++) {
                                $('#' + [key] +'_c_b_l_error').append(response.internal_feeds[key].country_blacklist);
                            }

                        }
                        if(response.internal_feeds[key].id_whitelist)
                        {
                            $('#' + [key] +'_id_w_l_error').html('');
                            document.getElementById('id_w_l' + [key]).classList.add('is-invalid');
                            $('#' + [key] +'_id_w_l_error').css('display', 'block');
                            for(var i = 0; i < response.internal_feeds[key].id_whitelist.length; i++) {
                                $('#' + [key] +'_id_w_l_error').append(response.internal_feeds[key].id_whitelist);
                            }

                        }
                        if(response.internal_feeds[key].id_blacklist)
                        {
                            $('#' + [key] +'_id_b_l_error').html('');
                            document.getElementById('id_b_l' + [key]).classList.add('is-invalid');
                            $('#' + [key] +'_id_b_l_error').css('display', 'block');
                            for(var i = 0; i < response.internal_feeds[key].id_blacklist.length; i++) {
                                $('#' + [key] +'_id_b_l_error').append(response.internal_feeds[key].id_blacklist);
                            }

                        }
                        if(response.internal_feeds[key].url_whitelist)
                        {
                            $('#' + [key] +'_url_w_l_error').html('');
                            document.getElementById('url_w_l' + [key]).classList.add('is-invalid');
                            $('#' + [key] +'_url_w_l_error').css('display', 'block');
                            for(var i = 0; i < response.internal_feeds[key].url_whitelist.length; i++) {
                                $('#' + [key] +'_url_w_l_error').append(response.internal_feeds[key].url_whitelist);
                            }

                        }
                        if(response.internal_feeds[key].url_blacklist)
                        {
                            $('#' + [key] +'_url_b_l_error').html('');
                            document.getElementById('url_b_l' + [key]).classList.add('is-invalid');
                            $('#' + [key] +'_url_b_l_error').css('display', 'block');
                            for(var i = 0; i < response.internal_feeds[key].url_blacklist.length; i++) {
                                $('#' + [key] +'_url_b_l_error').append(response.internal_feeds[key].url_blacklist);
                            }
                        }
                    }
                }
            },
            success: function(data) {
                window.location.href = "{{route('externalFeeds.web.index')}}"
            }
        });
        return false;
     });

    });


</script>
<!--end::Page Scripts -->
@endpush
