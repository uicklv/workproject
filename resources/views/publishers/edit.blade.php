@extends('layout')

@section('title', 'Edit Publisher')

@section('content')
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                               Edit Publisher
                            </h3>
                            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                            <div class="kt-subheader__group" id="kt_subheader_search">
										<span class="kt-subheader__desc" id="kt_subheader_total">
											Enter publisher details and submit </span>
                            </div>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <a href="#" class="btn btn-default btn-bold">
                                Back </a>
                            <div class="btn-group">
                                <button type="button" class="btn btn-brand btn-bold">
                                    Submit </button>
                                <button type="button" class="btn btn-brand btn-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-writing"></i>
                                                <span class="kt-nav__link-text">Save &amp; continue</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-medical-records"></i>
                                                <span class="kt-nav__link-text">Save &amp; add new</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-hourglass-1"></i>
                                                <span class="kt-nav__link-text">Save &amp; exit</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    <div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">


                        <div class="kt-portlet">
                            <div class="kt-portlet__body kt-portlet__body--fit">
                                <div class="kt-grid">
                                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">

                                        <!--begin: Form Wizard Form-->
                                       <form class="kt-form" id="my_form">
                                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                <div class="kt-heading kt-heading--md"><h3>Publisher Details:</h3></div>
                                                <div class="kt-section kt-section--first">
                                                    <div class="kt-wizard-v4__form">
                                                        <div class="row">
                                                            <div class="col-xl-12">
                                                                <div class="kt-section__body">
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Name</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <input type="text" class="form-control"  name="name" id="name" value="{{old('name', $publisher->name)}}" >
                                                                            <div class="invalid-feedback" id="name_error"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Company</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <input type="text" class="form-control"  name="company" id="company" value="{{old('company', $publisher->company)}}" >
                                                                            <div class="invalid-feedback" id="company_error"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label">Country</label>
                                                                        <div class="col-lg-9 col-xl-9">
                                                                            <input type="text" class="tags"  name="country" id="country" value="{{old('country', $publisher->country)}}" >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="kt-login__actions">
                                                <button id="kt_login_signin_submit" type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                           @csrf
                                        </form>
                                        <!--end: Form Wizard Form-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

        </div>
    </div>
</div>

<!-- end:: Page -->

@endsection
@push('css')
    <link href="/assets/css/pages/wizard/wizard-4.css" rel="stylesheet" type="text/css" />
@endpush

<!-- begin::Global Config(global config for global JS sciprts) -->
@push('scripts')
<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#2c77f4",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->
<!--begin::Global Theme Bundle(used by all pages) -->


<!--end::Global Theme Bundle -->

<!--begin::Page Scripts(used by this page) -->
<script src="/assets/js/pages/custom/user/add-user.js" type="text/javascript"></script>
<script src="/tagify.js" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function(){





    $('#my_form').submit(function() {


        var request = {};
        var name = document.getElementById('name').value;
        var country = document.getElementById('country').value;
        var company = document.getElementById('company').value;

        request['name'] = name;
        request['country'] = country;
        request['company'] = company;



        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $.ajax({
            url:  '{{ route('publishers.update', $publisher->id) }}',
            type: 'PUT',
            data: request,
            error: function(xhr) {
                var response = jQuery.parseJSON(xhr.responseText);
                if(response.errors.name)
                {
                    $('#name_error').html('');
                    document.getElementById('name').classList.add('is-invalid');
                    $('#name_error').css('display', 'block');
                    for(var i = 0; i < response.errors.name.length; i++) {
                        $('#name_error').append(response.errors.name[i]);
                    }
                }
                if(response.errors.company)
                {
                    $('#company_error').html('');
                    document.getElementById('company').classList.add('is-invalid');
                    $('#company_error').css('display', 'block');
                    for(var i = 0; i < response.errors.company.length; i++) {
                        $('#company_error').append(response.errors.company[i]);
                    }
                }
                if(response.errors.country)
                {
                    document.getElementById('country').classList.add('is-invalid');
                }

            },
            success: function(data) {
                window.location.href = "{{route('publishers.web.index')}}"
            }
        });
        return false;
     });

    });


</script>
@endpush
