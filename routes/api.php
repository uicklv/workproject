<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:sanctum')->prefix('/publishers')->group(function() {

    Route::get('/', '\\' . \App\Http\Controllers\PublisherApiController::class . '@index')->name('publishers.index');
    Route::post('/', '\\' . \App\Http\Controllers\PublisherApiController::class . '@store')->name('publishers.store');
    Route::get('/{publisher}', '\\' . \App\Http\Controllers\PublisherApiController::class . '@show')->name('publishers.show');
    Route::match(['put', 'patch'] ,'/{publisher}/edit', '\\' . \App\Http\Controllers\PublisherApiController::class . '@update')->name('publishers.update');
    Route::delete('/{publisher}', '\\' . \App\Http\Controllers\PublisherApiController::class . '@destroy')->name('publishers.destroy');

});


Route::middleware('auth:sanctum')->prefix('/advertisers')->group(function() {

    Route::get('/', '\\' . \App\Http\Controllers\AdvertiserApiController::class . '@index')->name('advertisers.index');
    Route::post('/', '\\' . \App\Http\Controllers\AdvertiserApiController::class . '@store')->name('advertisers.store');
    Route::get('/{advertiser}', '\\' . \App\Http\Controllers\AdvertiserApiController::class . '@show')->name('advertiser.show');
    Route::match(['put', 'patch'] ,'/{advertiser}/edit', '\\' . \App\Http\Controllers\AdvertiserApiController::class . '@update')->name('advertisers.update');
    Route::delete('/{advertiser}', '\\' . \App\Http\Controllers\AdvertiserApiController::class . '@destroy')->name('advertiser.destroy');

});

Route::middleware('auth:sanctum')->prefix('/internalFeeds')->group(function() {

    Route::get('/', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@index')->name('internalFeeds.index');
    Route::post('/', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@store')->name('internalFeeds.store');
    Route::get('/{internalFeed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@show')->name('internalFeeds.show');
    Route::match(['put', 'patch'] ,'/{internalFeed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@update')->name('internalFeeds.update');
    Route::get('/activation/{internalFeed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@activation')->name('internalFeeds.activation');
    Route::get('/deactivation/{internalFeed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@deactivation')->name('internalFeeds.deactivation');
    Route::delete('/{internalFeed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@destroy')->name('internalFeeds.destroy');

});


Route::middleware('auth:sanctum')->prefix('/externalFeeds')->group(function() {

    Route::get('/', '\\' . \App\Http\Controllers\External_feedApiController::class . '@index')->name('externalFeeds.index');
    Route::post('/', '\\' . \App\Http\Controllers\External_feedApiController::class . '@store')->name('externalFeeds.store');
    //Route::get('/{external_feed}', '\\' . \App\Http\Controllers\External_feedApiController::class . '@show')->name('external_feed.show');
    Route::match(['put', 'patch'] ,'/{externalFeed}', '\\' . \App\Http\Controllers\External_feedApiController::class . '@update')->name('externalFeeds.update');
    //Route::get('/activation/{external_feed}', '\\' . \App\Http\Controllers\External_feedApiController::class . '@activation')->name('external_feed.activation');
    //Route::get('/deactivation/{external_feed}', '\\' . \App\Http\Controllers\External_feedApiController::class . '@deactivation')->name('external_feed.deactivation');
    //Route::delete('/{external_feed}', '\\' . \App\Http\Controllers\External_feedApiController::class . '@destroy')->name('external_feed.destroy');

});


Route::middleware('auth:sanctum')->prefix('/internal_feed_settings')->group(function() {

    Route::get('/', '\\' . \App\Http\Controllers\Internal_feed_settingController::class . '@index')->name('internal_feed_settings.index');
    Route::post('/', '\\' . \App\Http\Controllers\Internal_feed_settingController::class . '@store')->name('internal_feed_setting.store');
    Route::get('/{internal_feed_setting}', '\\' . \App\Http\Controllers\Internal_feed_settingController::class . '@show')->name('internal_feed_setting.show');
    Route::match(['put', 'patch'] ,'/{internal_feed_setting}/edit', '\\' . \App\Http\Controllers\Internal_feed_settingController::class . '@update')->name('internal_feed_setting.update');
    Route::get('/activation/{internal_feed_setting}', '\\' . \App\Http\Controllers\Internal_feed_settingController::class . '@activation')->name('internal_feed_setting.activation');
    Route::get('/deactivation/{internal_feed_setting}', '\\' . \App\Http\Controllers\Internal_feed_settingController::class . '@deactivation')->name('internal_feed_setting.deactivation');
    Route::delete('/{internal_feed_setting}', '\\' . \App\Http\Controllers\Internal_feed_settingController::class . '@destroy')->name('internal_feed_setting.destroy');

});

Route::get('/codes', function (){

        $data = DB::select('SELECT value, code FROM country_codes');
        return json_encode($data);
});
