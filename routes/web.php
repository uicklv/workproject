<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/login', '\\' . \App\Http\Controllers\LoginController::class . '@index');
Route::post('/login', '\\' . \App\Http\Controllers\LoginController::class . '@handle')->name('login');


Route::get('/logout', '\\' . \App\Http\Controllers\LoginController::class . '@logout')
    ->name('logout')
    ->middleware('auth');


Route::get('/', function (){
    return view('dashboard');
})->name('dashboard')->middleware('auth:sanctum');


Route::middleware('auth:sanctum')->prefix('/publishers')->group(function() {

    Route::get('/', '\\' . \App\Http\Controllers\PublisherController::class . '@index')->name('publishers.web.index');
    Route::get('/create', '\\' . \App\Http\Controllers\PublisherController::class . '@create')->name('publishers.web.create');
    Route::get('/{publisher}/edit', '\\' . \App\Http\Controllers\PublisherController::class . '@edit')->name('publisher.web.edit');
   // Route::post('/', '\\' . \App\Http\Controllers\PublisherController::class . '@store')->name('publisher.store');
   // Route::get('/{publisher}', '\\' . \App\Http\Controllers\PublisherController::class . '@show')->name('publisher.show');
   // Route::match(['put', 'patch'] ,'/{publisher}/edit', '\\' . \App\Http\Controllers\PublisherController::class . '@update')->name('publisher.update');
   // Route::delete('/{publisher}', '\\' . \App\Http\Controllers\PublisherController::class . '@destroy')->name('publisher.destroy');

});

Route::middleware('auth:sanctum')->prefix('/advertisers')->group(function() {

      Route::get('/', '\\' . \App\Http\Controllers\AdvertiserController::class . '@index')->name('advertisers.web.index');
      Route::get('/create', '\\' . \App\Http\Controllers\AdvertiserController::class . '@create')->name('advertisers.web.create');
    Route::get('/{advertiser}/edit', '\\' . \App\Http\Controllers\AdvertiserController::class . '@edit')->name('advertiser.web.edit');

});

Route::middleware('auth:sanctum')->prefix('/internalFeeds')->group(function() {

    Route::get('/', '\\' . \App\Http\Controllers\Internal_feedController::class . '@index')->name('internalFeeds.web.index');
    Route::get('/create', '\\' . \App\Http\Controllers\Internal_feedController::class . '@create')->name('internalFeeds.web.create');
    Route::get('/{internalFeed}/edit', '\\' . \App\Http\Controllers\Internal_feedController::class . '@edit')->name('internalFeeds.edit');
    //Route::post('/', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@store')->name('internal_feed.store');
    //Route::get('/{internal_feed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@show')->name('internal_feed.show');
    //Route::match(['put', 'patch'] ,'/{internal_feed}/edit', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@update')->name('internal_feed.edit');
    //Route::get('/activation/{internal_feed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@activation')->name('internal_feed.activation');
    //Route::get('/deactivation/{internal_feed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@deactivation')->name('internal_feed.deactivation');
    //Route::delete('/{internal_feed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@destroy')->name('internal_feed.destroy');

});


Route::middleware('auth:sanctum')->prefix('/externalFeeds')->group(function() {

    Route::get('/', '\\' . \App\Http\Controllers\External_feedController::class . '@index')->name('externalFeeds.web.index');
    Route::get('/create', '\\' . \App\Http\Controllers\External_feedController::class . '@create')->name('externalFeeds.web.create');
    Route::get('/{externalFeed}/edit', '\\' . \App\Http\Controllers\External_feedController::class . '@edit')->name('internalFeeds.web.edit');
    //Route::post('/', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@store')->name('internal_feed.store');
    //Route::get('/{internal_feed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@show')->name('internal_feed.show');
    //Route::match(['put', 'patch'] ,'/{internal_feed}/edit', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@update')->name('internal_feed.update');
    //Route::get('/activation/{internal_feed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@activation')->name('internal_feed.activation');
    //Route::get('/deactivation/{internal_feed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@deactivation')->name('internal_feed.deactivation');
    //Route::delete('/{internal_feed}', '\\' . \App\Http\Controllers\Internal_feedApiController::class . '@destroy')->name('internal_feed.destroy');

});

Route::get('/test', function (){
    \Illuminate\Support\Facades\Redis::set('data', 'Hello');

    $data = \Illuminate\Support\Facades\Redis::get('data');
    dd($data);
});
