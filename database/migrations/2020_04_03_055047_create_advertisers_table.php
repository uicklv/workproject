<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisers', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name', 255);
            $table->char('country', 2);
            $table->string('company', 255);
            $table->uuid('manager_id');
            $table->integer('qps_limit');
            $table->string('timezone', 255);
            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->foreign('manager_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisers');
    }
}
