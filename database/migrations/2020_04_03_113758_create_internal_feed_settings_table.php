<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternalFeedSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_feed_settings', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('internal_feed_id');
            $table->uuid('external_feed_id');
            $table->json('country_whitelist');
            $table->json('country_blacklist');
            $table->json('source_id_whitelist');
            $table->json('source_id_blacklist');
            $table->json('source_url_whitelist');
            $table->json('source_url_blacklist');
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->foreign('internal_feed_id')->references('id')->on('internal_feeds');
            $table->foreign('external_feed_id')->references('id')->on('external_feeds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_feed_settings');
    }
}
