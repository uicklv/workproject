<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_feeds', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('advertiser_id');
            $table->string('name', 255);
            $table->string('category', 255);
            $table->json('parameters');
            $table->json('country_whitelist');
            $table->json('country_blacklist');
            $table->json('source_id_whitelist');
            $table->json('source_id_blacklist');
            $table->json('source_url_whitelist');
            $table->json('source_url_blacklist');
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->foreign('advertiser_id')->references('id')->on('advertisers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_feeds');
    }
}
