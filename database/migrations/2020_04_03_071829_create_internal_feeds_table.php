<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternalFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_feeds', function (Blueprint $table) {

            $table->uuid('id');
            $table->uuid('publisher_id');
            $table->string('name', 255);
            $table->string('category', 255);
            $table->json('parameters');
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->foreign('publisher_id')->references('id')->on('publishers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_feeds');
    }
}
