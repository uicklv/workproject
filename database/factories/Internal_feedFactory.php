<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\InternalFeed::class, function (Faker $faker) {

    $param = ['param' => 15];
    return [
        'id' => $faker->uuid,
        'publisher_id' => \App\Publisher::inRandomOrder()->limit(1)->get()->first()->id,
        'name' => $faker->company,
        'parameters' => json_encode($param),
        'is_active' => $faker->boolean,
    ];
});
