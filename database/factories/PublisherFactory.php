<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Publisher::class, function (Faker $faker) {
    return [
        'id' => $faker->uuid,
        'name' => $faker->name,
        'country' => $faker->countryCode,
        'company' => $faker->company,
        'manager_id' => \App\User::inRandomOrder()->limit(1)->get()->first()->id,
    ];
});
