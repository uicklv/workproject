<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\External_feed::class, function (Faker $faker) {


    $param = ['param' => 15];

    $country_whitelist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $country_whitelist[$i] = $faker->countryCode;
    }

    $country_blacklist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $country_blacklist[$i] = $faker->countryCode;
    }

    $source_id_whitelist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $source_id_whitelist[$i] = $faker->uuid;
    }

    $source_id_blacklist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $source_id_blacklist[$i] = $faker->uuid;
    }

    $source_url_whitelist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $source_url_whitelist[$i] = $faker->url;
    }

    $source_url_blacklist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $source_url_blacklist[$i] = $faker->url;
    }



    return [
        'id' => $faker->uuid,
        'advertiser_id' => \App\Advertiser::inRandomOrder()->limit(1)->get()->first()->id,
        'name' => $faker->name,
        'parameters' => json_encode($param),
        'country_whitelist' => json_encode($country_whitelist),
        'country_blacklist' => json_encode($country_blacklist),
        'source_id_whitelist' => json_encode($source_id_whitelist),
        'source_id_blacklist' => json_encode($source_id_blacklist),
        'source_url_whitelist' => json_encode($source_url_whitelist),
        'source_url_blacklist' => json_encode($source_id_blacklist),
        'is_active' => $faker->boolean,
    ];
});
