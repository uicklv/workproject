<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;

$factory->define(\App\InternalFeedSetting::class, function (Faker $faker) {
    $country_whitelist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $country_whitelist[$i] = $faker->countryCode;
    }

    $country_blacklist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $country_blacklist[$i] = $faker->countryCode;
    }

    $source_id_whitelist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $source_id_whitelist[$i] = $faker->uuid;
    }

    $source_id_blacklist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $source_id_blacklist[$i] = $faker->uuid;
    }

    $source_url_whitelist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $source_url_whitelist[$i] = $faker->url;
    }

    $source_url_blacklist = [];
    for ($i = 0; $i < 5; $i++)
    {
        $source_url_blacklist[$i] = $faker->url;
    }



    return [
        'id' => $faker->uuid,
        'internal_feed_id' => \App\InternalFeed::inRandomOrder()->limit(1)->get()->first()->id,
        'external_feed_id' => \App\External_feed::inRandomOrder()->limit(1)->get()->first()->id,
        'country_whitelist' => json_encode($country_whitelist),
        'country_blacklist' => json_encode($country_blacklist),
        'source_id_whitelist' => json_encode($source_id_whitelist),
        'source_id_blacklist' => json_encode($source_id_blacklist),
        'source_url_whitelist' => json_encode($source_url_whitelist),
        'source_url_blacklist' => json_encode($source_url_blacklist),
        'is_active' => $faker->boolean,
    ];
});
