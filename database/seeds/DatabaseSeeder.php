<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 30)->create();
        //factory(\App\Publisher::class, 30)->create();
        //factory(\App\Advertiser::class, 30)->create();
        //factory(\App\External_feed::class, 30)->create();
        factory(\App\Publisher::class, 5)->create()->each(function ($publisher) {
            $publisher->internal_feeds()->save(factory(\App\InternalFeed::class)->make());
        });
        factory(\App\Advertiser::class, 5)->create()->each(function ($advertiser) {
            $advertiser->external_feeds()->save(factory(\App\External_feed::class)->make());
        });
        factory(\App\InternalFeedSetting::class, 50)->create();
    }
}
