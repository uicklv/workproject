// Class definition
var KTSelect2 = function() {
    // Private functions
    var demos = function() {
        // basic
        $('#kt_select2_1, #kt_select2_1_validate').select2({
            placeholder: "Select a state"
        });


        $("#kt_select2_1").select2({

            ajax: {
                type: 'GET',
                url: "http://localhost/api/internal_feeds",
                data: function (params) {
                    return {
                        q: params.term, // search term
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used

                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            minimumInputLength: 1,

        });



    };



    // Public functions
    return {
        init: function() {
            demos();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    KTSelect2.init();
});


//# sourceURL=webpack:///../src/assets/js/pages/crud/forms/widgets/select2.js?
