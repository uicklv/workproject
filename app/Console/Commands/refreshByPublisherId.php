<?php

namespace App\Console\Commands;

use App\Publisher;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class refreshByPublisherId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:publisher {publisher}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $publisher_id = $this->argument('publisher');
        $publisher = Publisher::find($publisher_id);
        foreach($publisher->internal_feeds as $internal)
        {
            $id = $internal->id;
            Artisan::call('refresh:internal ' . $id);
        }
    }
}
