<?php

namespace App\Console\Commands;

use App\InternalFeed;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class refreshByInternalId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:internal {internalFeed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('internalFeed');
        $internalFeed = InternalFeed::find($id);

        $json_data = [];
        //manager
        $user = Auth::user();
        $json_data['internal_feed_id'] = $internalFeed->id;
        $json_data['publisher_id'] = $internalFeed->publisher->id;
        $json_data['payout'] = 60;
        $json_data['manager_id'] = $user->id;
        $json_data['external_feeds'] = [];

        foreach($internalFeed->internal_feed_settings as $setting)
        {
            $external_data = [];
            $external = $setting->external_feed;
            $external_data['advertiser_id'] = $external->advertiser_id;
            $external_data['external_feed_id'] = $external->id;
            $external_data['payout'] = 60;
            $external_data['country_whitelist'] = array_values(array_unique(array_merge(json_decode($setting->country_whitelist),json_decode($external->country_whitelist))));
            $external_data['country_blacklist'] = array_values(array_unique(array_merge(json_decode($setting->country_blacklist),json_decode($external->country_blacklist))));
            $external_data['source_id_whitelist'] = array_values(array_unique(array_merge(json_decode($setting->source_id_whitelist),json_decode($external->source_id_whitelist))));
            $external_data['source_id_blacklist'] = array_values(array_unique(array_merge(json_decode($setting->source_id_blacklist),json_decode($external->source_id_blacklist))));
            $external_data['source_url_whitelist'] = array_values(array_unique(array_merge(json_decode($setting->source_url_whitelist),json_decode($external->source_url_whitelist))));
            $external_data['source_url_blacklist'] = array_values(array_unique(array_merge(json_decode($setting->source_url_blacklist),json_decode($external->source_url_blacklist))));

            if($parameters = json_decode($external->parameters)){
                foreach ($parameters as $k => $p)
                {
                    $external_data[$k] = $p;
                }
            }
            array_push($json_data['external_feeds'], $external_data);
        }

        //set json in Redis
        Redis::set($internalFeed->id, json_encode($json_data));

    }
}
