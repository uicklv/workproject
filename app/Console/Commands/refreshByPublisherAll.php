<?php

namespace App\Console\Commands;

use App\Publisher;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class refreshByPublisherAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:publisher_all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $publishers = Publisher::all();

        foreach ($publishers as $pub)
        {
            $id = $pub->id;
            Artisan::call('refresh:publisher ' . $id);
        }

    }
}
