<?php


namespace App\Http\Controllers;




use App\InternalFeedSetting;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class Internal_feed_settingController
{
    public function index()
    {
        $internal_feed_settings = InternalFeedSetting::latest('id')->paginate(5);
        return $internal_feed_settings;
    }

    public function store(Request $request)
    {

        $request->validate([
            'internal_id' => 'required|uuid',
            'external_id' => 'required|uuid',
            'country_whitelist' => 'required|json',
            'country_blacklist' => 'required|json',
            'source_id_whitelist' => 'required|json',
            'source_id_blacklist' => 'required|json',
            'source_url_whitelist' => 'required|json',
            'source_url_blacklist' => 'required|json',
            'is_active' => 'required|boolean',
        ]);

        $internal_feed_setting = new InternalFeedSetting();
        $internal_feed_setting->id = Uuid::uuid4()->toString();
        $internal_feed_setting->internal_id = $request->get('internal_id');
        $internal_feed_setting->external_id = $request->get('external_id');
        $internal_feed_setting->country_whitelist = $request->get('country_whitelist');
        $internal_feed_setting->country_blacklist = $request->get('country_blacklist');
        $internal_feed_setting->source_id_whitelist = $request->get('source_id_whitelist');
        $internal_feed_setting->source_id_blacklist = $request->get('source_id_blacklist');
        $internal_feed_setting->source_url_whitelist = $request->get('source_url_whitelist');
        $internal_feed_setting->source_url_blacklist = $request->get('source_url_blacklist');
        $internal_feed_setting->is_active = $request->get('is_active');
        $internal_feed_setting->save();

        return $internal_feed_setting;
    }

    public function show(InternalFeedSetting $internal_feed_setting)
    {
        return $internal_feed_setting;
    }

    public function update(InternalFeedSetting $internal_feed_setting, Request $request)
    {

        $request->validate([
            'internal_id' => 'required|uuid',
            'external_id' => 'required|uuid',
            'country_whitelist' => 'required|json',
            'country_blacklist' => 'required|json',
            'source_id_whitelist' => 'required|json',
            'source_id_blacklist' => 'required|json',
            'source_url_whitelist' => 'required|json',
            'source_url_blacklist' => 'required|json',
            'is_active' => 'required|boolean',
        ]);

        $internal_feed_setting->internal_id = $request->get('internal_id');
        $internal_feed_setting->external_id = $request->get('external_id');
        $internal_feed_setting->country_whitelist = $request->get('country_whitelist');
        $internal_feed_setting->country_blacklist = $request->get('country_blacklist');
        $internal_feed_setting->source_id_whitelist = $request->get('source_id_whitelist');
        $internal_feed_setting->source_id_blacklist = $request->get('source_id_blacklist');
        $internal_feed_setting->source_url_whitelist = $request->get('source_url_whitelist');
        $internal_feed_setting->source_url_blacklist = $request->get('source_url_blacklist');
        $internal_feed_setting->is_active = $request->get('is_active');
        $internal_feed_setting->save();

        return $internal_feed_setting;

    }

    public function activation(InternalFeedSetting $internal_feed_setting)
    {

        $internal_feed_setting->is_active = 1;
        $internal_feed_setting->save();

        return $internal_feed_setting;
    }

    public function deactivation(InternalFeedSetting $internal_feed_setting)
    {

        $internal_feed_setting->is_active = 0;
        $internal_feed_setting->save();

        return $internal_feed_setting;
    }

    public function destroy(InternalFeedSetting $internal_feed_setting)
    {
        $internal_feed_setting->delete();
        return $internal_feed_setting;
    }

}
