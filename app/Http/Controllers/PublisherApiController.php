<?php


namespace App\Http\Controllers;


use App\Advertiser;
use App\Publisher;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class PublisherApiController
{
    public function index()
    {
        $builder = \App\Publisher::select('id', 'name', 'country','company', 'manager_id');
        //search
        if($query = request()->get('query')) {
            if (Uuid::isValid($query[0])) {
                $builder->where('id', '=', $query[0]);
            } else {
                $builder->where('name', 'LIKE', '%' . $query[0] . '%');
            }
        }

        //sort
        //$sort = request()->get('sort', ['field' => 'updated_at', 'sort' => 'desc']);
        //$field = $sort['field'];
        //$sorting = $sort['sort'];

        list("field" => $field, "sort" => $sorting) = request()->get('sort', ['field' => 'updated_at', 'sort' => 'desc']);

        $builder->orderBy($field, $sorting);

        //paginate
        $response = [];
        if ($pagination = request()->get('pagination')) {
            $paginator = $builder->paginate($pagination['perpage'], ['*'], 'pagination[page]', $pagination['page']);

            $response['data'] = $paginator->items();

            $response['meta'] = [
                'page' => $paginator->currentPage(),
                'pages'=> $paginator->lastPage(),
                'perpage'=> $paginator->perPage(),
                'total' => $paginator->total(),
                'sort' => $sorting,
                'field' => $field
            ];

            return $response;
        }
    }

    public function store()
    {
        request()->validate([
            'name' => 'required|max:255',
            'company' => 'required|max:255',
            'country' => 'required|json',
            'manager_id' => 'required|uuid'
        ]);


        $name = request()->get('name');
        $company = request()->get('company');
        foreach (json_decode(request()->get('country')) as $val)
        {
            $country = $val->code;
        }

        $manager_id = request()->get('manager_id');

        $publisher = new Publisher();
        $publisher->id = Uuid::uuid4();
        $publisher->name = $name;
        $publisher->country = $country;
        $publisher->company = $company;
        $publisher->manager_id = $manager_id;
        $publisher->save();

        //logging action
        $user = Auth::user();
        $data = serialize($publisher);
        Log::info($user->name . 'id: ' . $user->id . ' | created ' . 'advertiser: ' . $publisher->id .
            ' | data: ', ['data' => $data]);

        redirect()->route('publishers.web.index');
    }

    public function show(\App\Publisher $publisher)
    {
        return $publisher;
    }

    public function update(\App\Publisher $publisher)
    {
        request()->validate([
            'name' => 'required|max:255',
            'company' => 'required|max:255',
            'country' => 'required|json',
        ]);

        $publisher->name = request()->get('name');
        $publisher->country = request()->get('country');
        $publisher->company = request()->get('company');
        $publisher->save();

        //logging action
        $user = Auth::user();
        $data = serialize($publisher);
        Log::info($user->name . 'id: ' . $user->id . ' | updated ' . 'advertiser: ' . $publisher->id .
            ' | data: ', ['data' => $data]);

        redirect()->route('publishers.web.index');
    }

    public function destroy(\App\Publisher $publisher)
    {
        $publisher->delete();

        return $publisher;
    }

}
