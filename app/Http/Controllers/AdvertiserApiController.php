<?php


namespace App\Http\Controllers;


use App\Advertiser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class AdvertiserApiController
{

    public function index()
    {
        $builder = \App\Advertiser::select('id', 'name', 'country', 'company', 'manager_id', 'qps_limit');
        //search
        if ($query = request()->get('query')) {
            if (Uuid::isValid($query[0])) {
                $builder->where('id', '=', $query[0]);
            } else {
                $builder->where('name', 'LIKE', '%' . $query[0] . '%');
            }
        }

        //sort
        //$sort = request()->get('sort', ['field' => 'updated_at', 'sort' => 'desc']);
        //$field = $sort['field'];
        //$sorting = $sort['sort'];

        list("field" => $field, "sort" => $sorting) = request()->get('sort', ['field' => 'updated_at', 'sort' => 'desc']);

        $builder->orderBy($field, $sorting);

        //paginate
        $response = [];
        if ($pagination = request()->get('pagination')) {
            $paginator = $builder->paginate($pagination['perpage'], ['*'], 'pagination[page]', $pagination['page']);

            $response['data'] = $paginator->items();

            $response['meta'] = [
                'page' => $paginator->currentPage(),
                'pages' => $paginator->lastPage(),
                'perpage' => $paginator->perPage(),
                'total' => $paginator->total(),
                'sort' => $sorting,
                'field' => $field
            ];

            return $response;
        }
    }

    public function store()
    {
        request()->validate([
            'name' => 'required|max:255',
            'company' => 'required|max:255',
            'country' => 'required|json',
            'qps_limit' => 'required|integer',
            'timezone' => 'required|timezone',
            'manager_id' => 'required|uuid'
        ]);

        $name = request()->get('name');
        $company = request()->get('company');
        foreach (json_decode(request()->get('country')) as $val)
        {
            $country = $val->code;
        }
        $qps_limit = request()->get('qps_limit');
        $timezone = request()->get('timezone');
        $manager_id = request()->get('manager_id');

        $advertiser = new Advertiser();
        $advertiser->id = Uuid::uuid4();
        $advertiser->name = $name;
        $advertiser->country = $country;
        $advertiser->company = $company;
        $advertiser->manager_id = $manager_id;
        $advertiser->qps_limit = $qps_limit;
        $advertiser->timezone = $timezone;
        $advertiser->save();

        //logging action
        $user = Auth::user();
        $data = serialize($advertiser);
        Log::info($user->name . 'id: ' . $user->id . ' | created ' . 'advertiser: ' . $advertiser->id .
            ' | data: ', ['data' => $data]);


        redirect()->route('advertisers.web.index');

    }

    public function show(\App\Advertiser $advertiser)
    {
        return $advertiser;
    }

    public function update(\App\Advertiser $advertiser)
    {

        request()->validate([
            'name' => 'required|max:255',
            'company' => 'required|max:255',
            'country' => 'required|json',
            'qps_limit' => 'required|integer',
            'timezone' => 'required|timezone',
        ]);

        $advertiser->name = request()->get('name');
            foreach (json_decode(request()->get('country')) as $c) {
                $country = $c->code;
            }

        $advertiser->country = $country;
        $advertiser->company = request()->get('company');
        $advertiser->qps_limit = request()->get('qps_limit');
        $advertiser->timezone = request()->get('timezone');
        $advertiser->save();

        //logging action
        $user = Auth::user();
        $data = serialize($advertiser);
        Log::info($user->name . 'id: ' . $user->id . ' | updated' . 'advertiser: ' . $advertiser->id .
            ' | data: ', ['data' => $data]);

        redirect()->route('advertisers.web.index');
    }

    public function destroy(\App\Advertiser $advertiser)
    {
        $advertiser->delete();

        return $advertiser;
    }

}
