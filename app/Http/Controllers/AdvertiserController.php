<?php


namespace App\Http\Controllers;



use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class AdvertiserController
{

    public function index()
    {
        return view('advertisers.index');
    }

    public function create()
    {
        $manager_id = Auth::user()->getAuthIdentifier();
        $timezones = DB::select('SELECT name FROM timezones');
        return view('advertisers.create', ['manager_id' => $manager_id, 'timezones' => $timezones]);
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'country' => 'required|size:2',
            'manager_id' => 'required|uuid',
            'qps_limit' => 'required',
        ]);

        $advertiser = new \App\Advertiser();
        $advertiser->id = Uuid::uuid4()->toString();
        $advertiser->name = $request->get('name');
        $advertiser->country = $request->get('country');
        $advertiser->manager_id = $request->get('manager_id');
        $advertiser->qps_limit = $request->get('qps_limit');
        $advertiser->save();

        return $advertiser;
    }

    public function show(\App\Advertiser $advertiser)
    {
        return $advertiser;
    }

    public function edit(\App\Advertiser $advertiser)
    {
        $manager_id = Auth::user()->getAuthIdentifier();
        $timezones = DB::select('SELECT name FROM timezones');
        return view('advertisers.edit', ['advertiser' => $advertiser, 'timezones' => $timezones]);
    }

    public function update(\App\Advertiser $advertiser, Request $request)
    {

        $request->validate([
            'name' => 'required',
            'country' => 'required|size:2',
            'manager_id' => 'required|uuid',
            'qps_limit' => 'required',
        ]);

        $advertiser->id = $request->get('id');
        $advertiser->name = $request->get('name');
        $advertiser->country = $request->get('country');
        $advertiser->manager_id = $request->get('manager_id');
        $advertiser->qps_limit = $request->get('qps_limit');
        $advertiser->save();

        return $advertiser;
    }

    public function destroy(\App\Advertiser $advertiser)
    {
        $advertiser->delete();

        return $advertiser;
    }

}
