<?php


namespace App\Http\Controllers;


use App\ExternalFeed;
use App\InternalFeed;
use App\InternalFeedSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class External_feedApiController
{

    public function index()
    {

        $ExternalFeeds = \App\ExternalFeed::select('*')->whereHas('internalFeedSettings',function($query){
            return $query->whereHas('internalFeed', function($query){
                if ($q = request()->get('pub_id')) {
                    return $query->where('publisher_id', '=', $q);
                }
                return $query;
            });
        });
        list("field" => $field, "sort" => $sorting) = request()->get('sort', ['field' => 'external_feeds.updated_at', 'sort' => 'desc']);
        if ($field == 'id' || $field == 'advertiser_id') {
            $ExternalFeeds->orderBy($field, $sorting);
            }

        //filtr category
        if ($query = request()->get('category')) {
            $categories = [];
            foreach ($query as $q) {
                array_push($categories, $q);
            }
            $ExternalFeeds->whereIn('category', $categories);
        }

        //filtr status
        if ($query = request()->get('status')) {
            if ($query == 'active') {
                $query = 1;
            }elseif($query == 'inactive'){
                $query = 0;
            }
            $ExternalFeeds->where('is_active', '=', $query);
        }
        //search
        if ($query = request()->get('query')) {
            if (Uuid::isValid($query[0])) {
                $ExternalFeeds->where('id', '=', $query[0]);
            }
        }

        if ($pagination = request()->get('pagination')) {
            $paginator = $ExternalFeeds->paginate($pagination['perpage'], ['*'], 'pagination[page]', $pagination['page']);

            $response['meta'] = [
                'page' => $paginator->currentPage(),
                'pages' => $paginator->lastPage(),
                'perpage' => $paginator->perPage(),
                'total' => $paginator->total(),
                'sort' => $sorting,
                'field' => $field
            ];
        }
        foreach ($paginator as $feeds)
        {

            $pub_companies = [];
            foreach ($feeds->internalFeedSettings as $setting)
            {

                // $advert_companies[] = [
                //   'id' =>$setting->external_feed->advertiser->id,
                // 'company' =>$setting->external_feed->advertiser->company,
                //];
                $pub_companies[] = $setting->internalFeed->publisher->company;

            }
            if ($feeds->is_active == 1)
            {
                $status = 'active';
            }elseif($feeds->is_active == 0)
            {
                $status = 'inactive';
            }
            $data[] = [
                'id' => $feeds->id,
                'advertiser_id' => $feeds->advertiser_id,
                'company' =>   $feeds->advertiser->company,
                'is_active' => $status,
                'category' => $feeds->category,
                'pub_companies' => $pub_companies,
            ];
        }

        $response['data'] = $data;

        return $response;
    }

    public function store()
    {
        $errors = [];
        $validator = Validator::make(request()->all(), [
            'advert_id' => 'required|uuid',
            'name' => 'required|max:255',
            'category' => 'required|max:255',
            'parameters' => 'required',
            'is_active' => 'required|boolean',
            'country_whitelist' => 'required|json',
            'country_blacklist' => 'required|json',
            'id_whitelist' => 'required|json',
            'id_blacklist' => 'required|json',
            'url_whitelist' => 'required|json',
            'url_blacklist' => 'required|json',
        ]);

        if ($validator->fails()) {
            $errors[] = $validator->errors();

        }

        if ($internal_feeds = request()->get('internal_feeds'))
        {

            foreach ($internal_feeds as $internal_id => $params) {

                $v = Validator::make($params, [
                    'country_whitelist' => 'required|json',
                    'country_blacklist' => 'required|json',
                    'id_whitelist' => 'required|json',
                    'id_blacklist' => 'required|json',
                    'url_whitelist' => 'required|json',
                    'url_blacklist' => 'required|json',
                ]);

                if ($v->fails()) {
                    $errors['internal_feeds'][$internal_id] = $v->errors();
                }
            }

        }

        if($errors)
        {
            return response($errors, 403);
        }

        $advert_id = request()->get('advert_id');
        $name = request()->get('name');
        $category = request()->get('category');
        $parameters = request()->get('parameters');
        $is_active = request()->get('is_active');



        //country white list
        $country_wx = [];
        if ($white_l = json_decode(request()->get('country_whitelist'))) {
            foreach ($white_l as $countries) {
                //$country_w[] = $countries->code;
                $country_wx[] = $countries->value;
            }
        }


        //country black list
        $country_bx = [];
        if($black_l = json_decode(request()->get('country_blacklist'))) {
            foreach ($black_l as $countries) {
                //$country_b[] = $countries->code;
                $country_bx[] = $countries->value;
            }
        }


        //id white list
        $id_wx = [];
        if($id_wl = json_decode(request()->get('id_whitelist'))) {
            foreach ($id_wl as $id) {
                $id_wx[] = $id->value;
            }
        }
        //id black list
        $id_bx = [];
        if($id_bl = json_decode(request()->get('id_blacklist'))) {
            foreach ($id_bl as $id) {
                $id_bx[] = $id->value;
            }
        }
        //url white list
        $url_wx = [];
        if($url_wl = json_decode(request()->get('url_whitelist'))) {
            foreach ($url_wl as $url) {
                $url_wx[] = $url->value;
            }
        }
        //url black list
        $url_bx = [];
        if($url_bl = json_decode(request()->get('url_blacklist'))) {
            foreach ($url_bl as $url) {
                $url_bx[] = $url->value;
            }
        }


        $external_feeds = new ExternalFeed();
        $external_id = $external_feeds->id = Uuid::uuid4();
        $external_feeds->advertiser_id = $advert_id;
        $external_feeds->name = $name;
        $external_feeds->category = $category;
        $external_feeds->is_active = $is_active;
        $external_feeds->country_whitelist = json_encode($country_wx);
        $external_feeds->country_blacklist = json_encode($country_bx);
        $external_feeds->source_id_whitelist = json_encode($id_wx);
        $external_feeds->source_id_blacklist = json_encode($id_bx);
        $external_feeds->source_url_whitelist = json_encode($url_wx);
        $external_feeds->source_url_blacklist = json_encode($url_bx);
        $external_feeds->save();

        $all_data = [];
        $all_data['external_feed'] = $external_feeds;

        $all_id = [];
        if ($internal_feeds = request()->get('internal_feeds')) {

            foreach ($internal_feeds as $internal_id => $params) {


                //country white list
                $country_w = [];
                if ($white_l = json_decode($params['country_whitelist'])) {
                    foreach ($white_l as $countries) {
                        //$country_w[] = $countries->code;
                        $country_w[] = $countries->value;
                    }
                }

                //country black list
                $country_b = [];
                if ($black_l = json_decode($params['country_blacklist'])) {
                    foreach ($black_l as $countries) {
                        //$country_b[] = $countries->code;
                        $country_b[] = $countries->value;
                    }
                }

                //id white list
                $id_w = [];
                if ($id_wl = json_decode($params['id_whitelist'])) {
                    foreach ($id_wl as $id) {
                        $id_w[] = $id->value;
                    }
                }
                //id black list
                $id_b = [];
                if ($id_bl = json_decode($params['id_blacklist'])) {
                    foreach ($id_bl as $id) {
                        $id_b[] = $id->value;
                    }
                }
                //url white list
                $url_w = [];
                if ($url_wl = json_decode($params['url_whitelist'])) {
                    foreach ($url_wl as $url) {
                        $url_w[] = $url->value;
                    }
                }
                //url black list
                $url_b = [];
                if ($url_bl = json_decode($params['url_blacklist'])) {
                    foreach ($url_bl as $url) {
                        $url_b[] = $url->value;
                    }
                }

                $internal_feed_settings = new  \App\InternalFeedSetting();
                $internal_feed_settings->id = Uuid::uuid4();
                $internal_feed_settings->internal_feed_id = $internal_id;
                $internal_feed_settings->external_feed_id = $external_id;
                $internal_feed_settings->country_whitelist = json_encode($country_w);
                $internal_feed_settings->country_blacklist = json_encode($country_b);
                $internal_feed_settings->source_id_whitelist = json_encode($id_w);
                $internal_feed_settings->source_id_blacklist = json_encode($id_b);
                $internal_feed_settings->source_url_whitelist = json_encode($url_w);
                $internal_feed_settings->source_url_blacklist = json_encode($url_b);
                $internal_feed_settings->is_active = 1;
                $internal_feed_settings->save();

                $all_data['internal_settings'] = $internal_feed_settings;
                $all_id[] = $internal_feed_settings->id;

            }
        }


        //logging action
        $user = Auth::user();
        $data= serialize($all_data);
        Log::info($user->name . 'id: ' . $user->id . ' | created external feed:' . $external_feeds->id . ' | internal feed settings: ' . implode(',', $all_id) . ' data: ', ['data' => $data]);


        redirect()->route('externalFeeds.web.index');



    }

    public function show(\App\ExternalFeed $external_feed)
    {
        return $external_feed;
    }

    public function update(\App\ExternalFeed $externalFeed)
    {
        $errors = [];
        $validator = Validator::make(request()->all(), [
            'advertiser_id' => 'required|uuid',
            'name' => 'required|max:255',
            'category' => 'required|max:255',
            'parameters' => 'required',
            'is_active' => 'required|boolean',
            'country_whitelist' => 'required|json',
            'country_blacklist' => 'required|json',
            'id_whitelist' => 'required|json',
            'id_blacklist' => 'required|json',
            'url_whitelist' => 'required|json',
            'url_blacklist' => 'required|json',
        ]);

        if ($validator->fails()) {
            $errors[] = $validator->errors();

        }

        if ($internal_feeds = request()->get('internal_feeds'))
        {

            foreach ($internal_feeds as $internal_id => $params) {

                $v = Validator::make($params, [
                    'country_whitelist' => 'required|json',
                    'country_blacklist' => 'required|json',
                    'id_whitelist' => 'required|json',
                    'id_blacklist' => 'required|json',
                    'url_whitelist' => 'required|json',
                    'url_blacklist' => 'required|json',
                ]);

                if ($v->fails()) {
                    $errors['internal_feeds'][$internal_id] = $v->errors();
                }
            }

        }

        if($errors)
        {
            return response($errors, 403);
        }

        $advert_id =  request()->get('advertiser_id');
        $name = request()->get('name');
        $category = request()->get('category');
        $parameters = request()->get('parameters');
        $is_active = request()->get('is_active');

        //country white list
        $country_wx = [];
        if ($white_l = json_decode(request()->get('country_whitelist'))) {
            foreach ($white_l as $countries) {
                //$country_w[] = $countries->code;
                $country_wx[] = $countries->value;
            }
        }


        //country black list
        $country_bx = [];
        if($black_l = json_decode(request()->get('country_blacklist'))) {
            foreach ($black_l as $countries) {
                //$country_b[] = $countries->code;
                $country_bx[] = $countries->value;
            }
        }


        //id white list
        $id_wx = [];
        if($id_wl = json_decode(request()->get('id_whitelist'))) {
            foreach ($id_wl as $id) {
                $id_wx[] = $id->value;
            }
        }
        //id black list
        $id_bx = [];
        if($id_bl = json_decode(request()->get('id_blacklist'))) {
            foreach ($id_bl as $id) {
                $id_bx[] = $id->value;
            }
        }
        //url white list
        $url_wx = [];
        if($url_wl = json_decode(request()->get('url_whitelist'))) {
            foreach ($url_wl as $url) {
                $url_wx[] = $url->value;
            }
        }
        //url black list
        $url_bx = [];
        if($url_bl = json_decode(request()->get('url_blacklist'))) {
            foreach ($url_bl as $url) {
                $url_bx[] = $url->value;
            }
        }


        $externalFeed->advertiser_id = $advert_id;
        $externalFeed->name = $name;
        $externalFeed->category = $category;
       // $externalFeed->parameters = $parameters;
        $externalFeed->is_active = $is_active;
        $externalFeed->save();

        $all_data = [];
        $all_data['external_feed'] = $externalFeed;

       if($internal_feeds = request()->get('internal_feeds')) {

           $all_id = [];
           foreach ($internal_feeds as $settings_id => $params) {


               //country white list
               $country_w = [];
               if ($white_l = json_decode($params['country_whitelist'])) {
                   foreach ($white_l as $countries) {
                       //$country_w[] = $countries->code;
                       $country_w[] = $countries->value;
                   }
               }

               //country black list
               $country_b = [];
               if ($black_l = json_decode($params['country_blacklist'])) {
                   foreach ($black_l as $countries) {
                       //$country_b[] = $countries->code;
                       $country_b[] = $countries->value;
                   }
               }

               //id white list
               $id_w = [];
               if ($id_wl = json_decode($params['id_whitelist'])) {
                   foreach ($id_wl as $id) {
                       $id_w[] = $id->value;
                   }
               }
               //id black list
               $id_b = [];
               if ($id_bl = json_decode($params['id_blacklist'])) {
                   foreach ($id_bl as $id) {
                       $id_b[] = $id->value;
                   }
               }
               //url white list
               $url_w = [];
               if ($url_wl = json_decode($params['url_whitelist'])) {
                   foreach ($url_wl as $url) {
                       $url_w[] = $url->value;
                   }
               }
               //url black list
               $url_b = [];
               if ($url_bl = json_decode($params['url_blacklist'])) {
                   foreach ($url_bl as $url) {
                       $url_b[] = $url->value;
                   }
               }
               $internal_feed_settings = InternalFeedSetting::find($settings_id);
               $internal_feed_settings->country_whitelist = json_encode($country_w);
               $internal_feed_settings->country_blacklist = json_encode($country_b);
               $internal_feed_settings->source_id_whitelist = json_encode($id_w);
               $internal_feed_settings->source_id_blacklist = json_encode($id_b);
               $internal_feed_settings->source_url_whitelist = json_encode($url_w);
               $internal_feed_settings->source_url_blacklist = json_encode($url_b);
               $internal_feed_settings->is_active = $params['is_active'];
               $internal_feed_settings->save();

               $all_data['internal_settings'] = $internal_feed_settings;
               $all_id[] = $internal_feed_settings->id;

           }
       }

        //logging action
        $user = Auth::user();
        $data= serialize($all_data);
        Log::info($user->name . 'id: ' . $user->id . ' | updated external feed:' . $externalFeed->id . ' | internal feed settings: ' . implode(',', $all_id) . ' data: ', ['data' => $data]);

        redirect()->route('externalFeeds.web.index');

    }

    public function activation(\App\ExternalFeed $external_feed)
    {

        $external_feed->is_active = 1;
        $external_feed->save();

        return $external_feed;
    }

    public function deactivation(\App\ExternalFeed $external_feed)
    {

        $external_feed->is_active = 0;
        $external_feed->save();

        return $external_feed;
    }

    public function destroy(\App\ExternalFeed $external_feed)
    {
        $external_feed->delete();

        return $external_feed;
    }



}
