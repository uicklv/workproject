<?php


namespace App\Http\Controllers;


use App\Advertiser;
use App\InternalFeed;
use App\Publisher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class External_feedController
{

    public function index()
    {
        return view('external_feeds.index', ['publishers' => \App\Publisher::all()]);
    }

        public function create()
    {
        $advertisers = Advertiser::all();
        $internal_feeds = InternalFeed::all();
        $codes = DB::select('SELECT value, code FROM country_codes');
        return view('external_feeds.create', ['advertisers' => $advertisers, 'internal_feeds' => $internal_feeds, 'codes' => $codes]);
    }

    public function edit(\App\ExternalFeed $externalFeed)
    {
        $internalFeeds = [];
        foreach ($externalFeed->internalFeedSettings as $internal)
        {
            $internalFeeds[] = $internal;
        }
        return view('external_feeds.edit', ['externalFeed' => $externalFeed, 'internalFeeds' => $internalFeeds, 'advertisers' => Advertiser::all()]);
    }


    public function store(Request $request)
    {

        $request->validate([
            'advertiser_id' => 'required|uuid',
            'parameters' => 'required|json',
            'country_whitelist' => 'required|json',
            'country_blacklist' => 'required|json',
            'source_id_whitelist' => 'required|json',
            'source_id_blacklist' => 'required|json',
            'source_url_whitelist' => 'required|json',
            'source_url_blacklist' => 'required|json',
            'is_active' => 'required|boolean',
        ]);

        $external_feed = new \App\ExternalFeed();
        $external_feed->id = Uuid::uuid4()->toString();
        $external_feed->advertiser_id = $request->get('advertiser_id');
        $external_feed->parameters = $request->get('parameters');
        $external_feed->country_whitelist = $request->get('country_whitelist');
        $external_feed->country_blacklist = $request->get('country_blacklist');
        $external_feed->source_id_whitelist = $request->get('source_id_whitelist');
        $external_feed->source_id_blacklist = $request->get('source_id_blacklist');
        $external_feed->source_url_whitelist = $request->get('source_url_whitelist');
        $external_feed->source_url_blacklist = $request->get('source_url_blacklist');
        $external_feed->is_active = $request->get('is_active');
        $external_feed->save();

        return $external_feed;
    }

    public function show(\App\ExternalFeed $external_feed)
    {
        return $external_feed;
    }

    public function update(\App\ExternalFeed $external_feed, Request $request)
    {

        $request->validate([
            'advertiser_id' => 'required|uuid',
            'parameters' => 'required|json',
            'country_whitelist' => 'required|json',
            'country_blacklist' => 'required|json',
            'source_id_whitelist' => 'required|json',
            'source_id_blacklist' => 'required|json',
            'source_url_whitelist' => 'required|json',
            'source_url_blacklist' => 'required|json',
            'is_active' => 'required|boolean',
        ]);

        $external_feed->advertiser_id = $request->get('advertiser_id');
        $external_feed->parameters = $request->get('parameters');
        $external_feed->country_whitelist = $request->get('country_whitelist');
        $external_feed->country_blacklist = $request->get('country_blacklist');
        $external_feed->source_id_whitelist = $request->get('source_id_whitelist');
        $external_feed->source_id_blacklist = $request->get('source_id_blacklist');
        $external_feed->source_url_whitelist = $request->get('source_url_whitelist');
        $external_feed->source_url_blacklist = $request->get('source_url_blacklist');
        $external_feed->is_active = $request->get('is_active');
        $external_feed->save();

        return $external_feed;

    }

    public function activation(\App\ExternalFeed $external_feed)
    {

        $external_feed->is_active = 1;
        $external_feed->save();

        return $external_feed;
    }

    public function deactivation(\App\ExternalFeed $external_feed)
    {

        $external_feed->is_active = 0;
        $external_feed->save();

        return $external_feed;
    }

    public function destroy(\App\ExternalFeed $external_feed)
    {
        $external_feed->delete();

        return $external_feed;
    }

}
