<?php


namespace App\Http\Controllers;


use App\ExternalFeed;
use App\InternalFeed;
use App\InternalFeedSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class Internal_feedApiController
{

    public function index()
    {

        $InternalFeeds = \App\InternalFeed::select('*')->whereHas('internal_feed_settings', function ($query) {
            return $query->whereHas('external_feed', function ($query) {
                if ($q = request()->get('advertiser_id')) {
                    return $query->where('advertiser_id', '=', $q);
                }
                return $query;
            });
        });


        list("field" => $field, "sort" => $sorting) = request()->get('sort', ['field' => 'external_feeds.updated_at', 'sort' => 'desc']);
        if ($field == 'id' || $field == 'publisher_id') {
            $InternalFeeds->orderBy($field, $sorting);
        }

        //filtr category
        if ($query = request()->get('category')) {
            $categories = [];
            foreach ($query as $q) {
               array_push($categories, $q);
            }
            $InternalFeeds->whereIn('category', $categories);
        }

        //search
        if ($query = request()->get('query')) {
            if (Uuid::isValid($query[0])) {
                $InternalFeeds->where('id', '=', $query[0]);
            }
        }
        //filtr status
        if ($query = request()->get('status')) {
            if ($query == 'active') {
                $query = 1;
            }elseif($query == 'inactive'){
                $query = 0;
            }
                $InternalFeeds->where('is_active', '=', $query);
        }

        if ($pagination = request()->get('pagination')) {
            $paginator = $InternalFeeds->paginate($pagination['perpage'], ['*'], 'pagination[page]', $pagination['page']);

            $response['meta'] = [
                'page' => $paginator->currentPage(),
                'pages' => $paginator->lastPage(),
                'perpage' => $paginator->perPage(),
                'total' => $paginator->total(),
                'sort' => $sorting,
                'field' => $field
            ];
        }

        foreach ($paginator as $feeds) {
            $advert_companies = [];
            foreach ($feeds->internal_feed_settings as $setting) {

                // $advert_companies[] = [
                //   'id' =>$setting->external_feed->advertiser->id,
                // 'company' =>$setting->external_feed->advertiser->company,
                //];
                $advert_companies[] = $setting->external_feed->advertiser->company;

            }

            if ($feeds->is_active == 1)
            {
                $status = 'active';
            }elseif($feeds->is_active == 0)
            {
                $status = 'inactive';
            }

            $data[] = [
                'id' => $feeds->id,
                'publisher_id' => $feeds->publisher_id,
                'company' => $feeds->publisher->company,
                'is_active' => $status,
                'category' => $feeds->category,
                'advert_companies' => $advert_companies,
            ];
        }

        $response['data'] = $data;

        return $response;

    }

    public function store()
    {
        $errors = [];
        $validator = Validator::make(request()->all(), [
            'pub_id' => 'required|uuid',
            'name' => 'required|max:255',
            'category' => 'required|max:255',
            'parameters' => 'required',
            'is_active' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            $errors[] = $validator->errors();
        }
        if ($external_feeds = request()->get('external_feeds')) {

            foreach ($external_feeds as $external_id => $params) {

                $v = Validator::make($params, [
                    'country_whitelist' => 'required|json',
                    'country_blacklist' => 'required|json',
                    'id_whitelist' => 'required|json',
                    'id_blacklist' => 'required|json',
                    'url_whitelist' => 'required|json',
                    'url_blacklist' => 'required|json',
                ]);

                if ($v->fails()) {
                    $errors['external_feeds'][$external_id] = $v->errors();
                }
            }

        }

        if ($errors) {
            return response($errors, 403);
        }

        $publisher_id = request()->get('pub_id');
        $name = request()->get('name');
        $category = request()->get('category');
        $parameters = request()->get('parameters');
        $is_active = request()->get('is_active');


        $internal_feeds = new InternalFeed();
        $int_id = $internal_feeds->id = Uuid::uuid4();
        $internal_feeds->publisher_id = $publisher_id;
        $internal_feeds->name = $name;
        $internal_feeds->category = $category;
        $internal_feeds->is_active = $is_active;
        $internal_feeds->save();

        $all_data = [];
        $all_data['internal_feed'] = $internal_feeds;



        if ($external_feeds = request()->get('external_feeds')) {
            $all_id = [];
            foreach ($external_feeds as $external_id => $params) {

                //country white list
                $country_w = [];
                if ($white_l = json_decode($params['country_whitelist'])) {
                    foreach ($white_l as $countries) {
                        //$country_w[] = $countries->code;
                        $country_w[] = $countries->value;
                    }
                }

                //country black list
                $country_b = [];
                if ($black_l = json_decode($params['country_blacklist'])) {
                    foreach ($black_l as $countries) {
                        //$country_b[] = $countries->code;
                        $country_b[] = $countries->value;
                    }
                }

                //id white list
                $id_w = [];
                if ($id_wl = json_decode($params['id_whitelist'])) {
                    foreach ($id_wl as $id) {
                        $id_w[] = $id->value;
                    }
                }
                //id black list
                $id_b = [];
                if ($id_bl = json_decode($params['id_blacklist'])) {
                    foreach ($id_bl as $id) {
                        $id_b[] = $id->value;
                    }
                }
                //url white list
                $url_w = [];
                if ($url_wl = json_decode($params['url_whitelist'])) {
                    foreach ($url_wl as $url) {
                        $url_w[] = $url->value;
                    }
                }
                //url black list
                $url_b = [];
                if ($url_bl = json_decode($params['url_blacklist'])) {
                    foreach ($url_bl as $url) {
                        $url_b[] = $url->value;
                    }
                }

                $internal_feed_settings = new  \App\InternalFeedSetting();
                $internal_feed_settings->id = Uuid::uuid4();
                $internal_feed_settings->internal_feed_id = $int_id;
                $internal_feed_settings->external_feed_id = $external_id;
                $internal_feed_settings->country_whitelist = json_encode($country_w);
                $internal_feed_settings->country_blacklist = json_encode($country_b);
                $internal_feed_settings->source_id_whitelist = json_encode($id_w);
                $internal_feed_settings->source_id_blacklist = json_encode($id_b);
                $internal_feed_settings->source_url_whitelist = json_encode($url_w);
                $internal_feed_settings->source_url_blacklist = json_encode($url_b);
                $internal_feed_settings->is_active = 1;
                $internal_feed_settings->save();


                $all_data['internal_settings'] = $internal_feed_settings;
                $all_id[] = $internal_feed_settings->id;

            }

        }

        $data = serialize($all_data);
        //manager
        $user = Auth::user();
        if ($external_feeds = request()->get('external_feeds')) {
            //logging action
            Log::info($user->name . 'id: ' . $user->id . ' | created internal feed:' . $internal_feeds->id .
                ' | internal feed settings: ' . implode(',', $all_id) . ' data: ', ['data' => $data]);
        }else{
            //logging action
            Log::info($user->name . 'id: ' . $user->id . ' | created internal feed:' . $internal_feeds->id . ' data: ', ['data' => $data]);
        }

        //set json in Redis
        \Illuminate\Support\Facades\Artisan::call('refresh:internal ' . $int_id);
        redirect()->route('internalFeeds.web.index');

    }

    public function show(\App\InternalFeed $internal_feed)
    {
        return $internal_feed;
    }

    public function update(InternalFeed $internalFeed)
    {
        $errors = [];
        $validator = Validator::make(request()->all(), [
            'pub_id' => 'required|uuid',
            'name' => 'required|max:255',
            'category' => 'required|max:255',
            'parameters' => 'required',
            'is_active' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            $errors[] = $validator->errors();

        }
        if ($external_feeds = request()->get('external_feeds')) {

            foreach ($external_feeds as $external_id => $params) {

                $v = Validator::make($params, [
                    'country_whitelist' => 'required|json',
                    'country_blacklist' => 'required|json',
                    'id_whitelist' => 'required|json',
                    'id_blacklist' => 'required|json',
                    'url_whitelist' => 'required|json',
                    'url_blacklist' => 'required|json',
                ]);

                if ($v->fails()) {
                    $errors['external_feeds'][$external_id] = $v->errors();
                }
            }

        }
        if ($errors) {
            return response($errors, 403);
        }


        $pub_id = request()->get('pub_id');
        $name = request()->get('name');
        $category = request()->get('category');
        $parameters = request()->get('parameters');
        $is_active = request()->get('is_active');

        $internalFeed->publisher_id = $pub_id;
        $internalFeed->name = $name;
        $internalFeed->category = $category;
        //  $internalFeed->parameters = $parameters;
        $internalFeed->is_active = $is_active;
        $internalFeed->save();

        $all_data = [];
        $all_data['internal_feed'] = $internalFeed;

        if ($external_feeds = request()->get('external_feeds')) {

            $all_id = [];
            foreach ($external_feeds as $settings_id => $params) {


                //country white list
                $country_w = [];
                if ($white_l = json_decode($params['country_whitelist'])) {
                    foreach ($white_l as $countries) {
                        //$country_w[] = $countries->code;
                        $country_w[] = $countries->value;
                    }
                }

                //country black list
                $country_b = [];
                if ($black_l = json_decode($params['country_blacklist'])) {
                    foreach ($black_l as $countries) {
                        //$country_b[] = $countries->code;
                        $country_b[] = $countries->value;
                    }
                }

                //id white list
                $id_w = [];
                if ($id_wl = json_decode($params['id_whitelist'])) {
                    foreach ($id_wl as $id) {
                        $id_w[] = $id->value;
                    }
                }
                //id black list
                $id_b = [];
                if ($id_bl = json_decode($params['id_blacklist'])) {
                    foreach ($id_bl as $id) {
                        $id_b[] = $id->value;
                    }
                }
                //url white list
                $url_w = [];
                if ($url_wl = json_decode($params['url_whitelist'])) {
                    foreach ($url_wl as $url) {
                        $url_w[] = $url->value;
                    }
                }
                //url black list
                $url_b = [];
                if ($url_bl = json_decode($params['url_blacklist'])) {
                    foreach ($url_bl as $url) {
                        $url_b[] = $url->value;
                    }
                }
                $internal_feed_settings = InternalFeedSetting::find($settings_id);
                $internal_feed_settings->country_whitelist = json_encode($country_w);
                $internal_feed_settings->country_blacklist = json_encode($country_b);
                $internal_feed_settings->source_id_whitelist = json_encode($id_w);
                $internal_feed_settings->source_id_blacklist = json_encode($id_b);
                $internal_feed_settings->source_url_whitelist = json_encode($url_w);
                $internal_feed_settings->source_url_blacklist = json_encode($url_b);
                $internal_feed_settings->is_active = $params['is_active'];
                $internal_feed_settings->save();

                $all_data['internal_settings'] = $internal_feed_settings;
                $all_id[] = $internal_feed_settings->id;

            }
        }

        //logging action
        $user = Auth::user();
        $data = serialize($all_data);
        Log::info($user->name . 'id: ' . $user->id . ' | updated internal feed:' . $internalFeed->id .
            ' | internal feed settings: ' . implode(',', $all_id) . ' data: ', ['data' => $data]);

        //set json data in Redis
        \Illuminate\Support\Facades\Artisan::call('refresh:internal ' . $internalFeed->id);

        redirect()->route('internalFeeds.web.index');

    }

    public function activation(\App\InternalFeed $internal_feed)
    {

        $internal_feed->is_active = 1;
        $internal_feed->save();

        return $internal_feed;
    }

    public function deactivation(\App\InternalFeed $internal_feed)
    {

        $internal_feed->is_active = 0;
        $internal_feed->save();

        return $internal_feed;
    }

    public function destroy(\App\InternalFeed $internal_feed)
    {
        $internal_feed->delete();

        return $internal_feed;
    }

}
