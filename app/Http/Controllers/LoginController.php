<?php


namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class LoginController
{
    public function index()
    {
        if(request()->user())
        {
            return view('dashboard');
        }
        return view('login');
    }

    public function handle()
    {
        if (request()->user()) {
            return response()->json([], 403);
        }

        $email = request()->get('email', null);
        $password = request()->get('password', null);
        $remember = request()->get('remember', 0);

        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
           $user = Auth::user();
           $token = $user->createToken('token-name');
           session()->put('token', $token->plainTextToken);
           return response()->json([], 200);
        }

        return response()->json([], 401);


    }

    public function logout()
    {
        Auth::user()->tokens()->delete();
        Auth::logout();
        return redirect()->route('login');
    }


}
