<?php


namespace App\Http\Controllers;


use App\Publisher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublisherController
{

    public function index()
    {
        return view('publishers.index');
    }

    public function create()
    {
        $manager_id = Auth::user()->getAuthIdentifier();
        return view('publishers.create', ['manager_id' => $manager_id]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'country' => 'required',
        ]);

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->email_verified_at = (new \DateTime())->format('Y-m-d H:i:s');
        $user->password = Hash::make($request->get('password'));
        $user->remember_token = Str::random(10);
        $user->save();
        return redirect()->route('users.index')
            ->with('message', 'user ' . $user->name . ' created');
    }

    public function show(\App\User $user)
    {

    }

    public function edit(\App\Publisher $publisher)
    {

        return view('publishers.edit', ['publisher' => $publisher]);
    }

    public function update(\App\User $user, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email, ' . $user->id . ',id',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->save();
        return redirect()->route('users.index')
            ->with('message', 'user ' . $user->name . ' updated');
    }

    public function destroy(\App\User $user)
    {
        $user->delete();

        return redirect()->route('users.index')
            ->with('message', 'user ' . $user->name . ' deleted');
    }

}
