<?php


namespace App\Http\Controllers;


use App\Advertiser;
use App\ExternalFeed;
use App\InternalFeed;
use App\Publisher;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class Internal_feedController
{

    public function index()
    {
        return view('internal_feeds.index', ['advertisers' => \App\Advertiser::all()]);
    }

    public function create()
    {
        $publishers = Publisher::all();
        $external_feeds = ExternalFeed::all();
        return view('internal_feeds.create', ['publishers' => $publishers, 'external_feeds' => $external_feeds]);
    }

    public function edit(\App\InternalFeed $internalFeed)
    {
        $externalFeeds = [];
        foreach ($internalFeed->internal_feed_settings as $external)
        {
            $externalFeeds[] = $external;
        }
        return view('internal_feeds.edit', ['internalFeed' => $internalFeed, 'externalFeeds' => $externalFeeds, 'publishers' => Publisher::all()]);
    }


    public function store(Request $request)
    {

        $request->validate([
            'publisher_id' => 'required|uuid',
            'parameters' => 'required|json',
            'is_active' => 'required|boolean',
        ]);

        $internal_feed = new \App\InternalFeed();
        $internal_feed->id = Uuid::uuid4()->toString();
        $internal_feed->publisher_id = $request->get('publisher_id');
        $internal_feed->parameters = $request->get('parameters');
        $internal_feed->is_active = $request->get('is_active');
        $internal_feed->save();

        return $internal_feed;
    }

    public function show(\App\InternalFeed $internal_feed)
    {
        return $internal_feed;
    }

    public function update(\App\InternalFeed $internal_feed, Request $request)
    {

        $request->validate([
            'publisher_id' => 'required|uuid',
            'parameters' => 'required|json',
            'is_active' => 'required|boolean',
        ]);

        $internal_feed->publisher_id = $request->get('publisher_id');
        $internal_feed->parameters = $request->get('parameters');
        $internal_feed->is_active = $request->get('is_active');
        $internal_feed->save();

        return $internal_feed;

    }

    public function activation(\App\InternalFeed $internal_feed)
    {

        $internal_feed->is_active = 1;
        $internal_feed->save();

        return $internal_feed;
    }

    public function deactivation(\App\InternalFeed $internal_feed)
    {

        $internal_feed->is_active = 0;
        $internal_feed->save();

        return $internal_feed;
    }

    public function destroy(\App\InternalFeed $internal_feed)
    {
        $internal_feed->delete();

        return $internal_feed;
    }

}
