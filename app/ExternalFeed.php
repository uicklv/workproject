<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExternalFeed extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'string';

    public function advertiser()
    {
        return $this->belongsTo(Advertiser::class);
    }

    public function internalFeedSettings()
    {
        return $this->hasMany(InternalFeedSetting::class);
    }

}
