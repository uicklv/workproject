<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'string';

    public function internal_feeds()
    {
        return $this->hasMany(InternalFeed::class);
    }

    public function manager()
    {
        return $this->belongsTo(User::class);
    }
}
