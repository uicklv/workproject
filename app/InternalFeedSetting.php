<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InternalFeedSetting extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'string';

    public function internalFeed()
    {
        return $this->belongsTo(InternalFeed::class);
    }

    public function external_feed()
    {
        return $this->belongsTo(ExternalFeed::class);
    }
}
