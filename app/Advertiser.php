<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advertiser extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'string';

    public function manager()
    {
        return $this->belongsTo(User::class);
    }

    public function external_feeds()
    {
        return $this->hasMany(\App\ExternalFeed::class);
    }
}
