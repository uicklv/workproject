<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InternalFeed extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'string';

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    public function internal_feed_settings()
    {
        return $this->hasMany(InternalFeedSetting::class);
    }
}
